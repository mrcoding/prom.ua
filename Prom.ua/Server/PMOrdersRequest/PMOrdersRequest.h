//
//  PMOrdersRequest.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMRequest.h"
#import "PMOrdersResponseModel.h"

@interface PMOrdersRequest : PMRequest

@property (nonatomic, strong) PMOrdersResponseModel *response;

@end
