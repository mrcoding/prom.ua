//
//  PMOrdersRequest.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersRequest.h"
#import "PMRequestProtectedMethods.h"
#import "PMOrdersRequestModel.h"
#import "PMItemModel.h"

static NSString * const kPMOrdersURL = @"cabinet/export_orders/xml/2007624";
static NSString * const kPMOrderrHashTag = @"72dfd843406955c15f9a427793182aad";

@implementation PMOrdersRequest

@dynamic response;

#pragma mark -
#pragma mark Private methods
- (NSDictionary *)paramsForRequest{
    PMOrdersRequestModel *requestModel = [PMOrdersRequestModel new];
    requestModel.hashTag = kPMOrderrHashTag;
    return [requestModel toDictionary];
}

- (NSString *)paramsURL{
    return kPMOrdersURL;
}

- (void)setupResponse:(id) response{
    NSError *error = nil;
    PMOrdersResponseModel *responseModel = [[PMOrdersResponseModel alloc] initWithDictionary:response error:&error];
    if (!error) {
        for (PMOrderModel *order in responseModel.order) {
            if (order.items) {
                order.items = [[PMItemsDict alloc] initWithDictionary:(NSDictionary *)(order.items) error:&error];
                if ([((PMItemsDict *)order.items).item isKindOfClass:[NSArray class]]) {
                    ((PMItemsDict *)order.items).item = [[PMItemModel arrayOfModelsFromDictionaries:(NSArray *)((PMItemsDict *)order.items).item error:&error] copy];
                } else if ([((PMItemsDict *)order.items).item isKindOfClass:[NSDictionary class]]) {
                    ((PMItemsDict *)order.items).item = (id)[[PMItemModel alloc] initWithDictionary:(NSDictionary *)(((PMItemsDict *)order.items).item) error:&error];
                }
            }
        }
        
        if (!error) {
            self.response = responseModel;
        } else {
            self.lastError = error;
        }
        
    } else {
        self.lastError = error;
    }
}

@end
