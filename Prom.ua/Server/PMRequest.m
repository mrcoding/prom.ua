//
//  PMRequest.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMRequest.h"
#import "PMKit.h"
#import <AFNetworking/AFNetworking.h>
#import <XMLDictionary/XMLDictionary.h>
#import "PMOrdersResponseModel.h"
#import "PMRequestProtectedMethods.h"

@implementation PMRequest

#pragma mark -
#pragma mark Class methods
+ (instancetype)request{
    return [self new];
}

#pragma mark -
#pragma mark Public methods
- (void)sendRequestWithCompletion:(PMRequestCompletionBlock) completion{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/xml"];
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer = responseSerializer;
    [manager GET:[self URLForRequest].absoluteString parameters:[self paramsForRequest]
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             
             responseObject = [NSDictionary dictionaryWithXMLData:responseObject];
             [self operateWithResponseObject:responseObject
                                       error:nil
                                  completion:completion];
             
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             
             [self operateWithResponseObject:nil
                                       error:error
                                  completion:completion];
             
         }];
}

#pragma mark -
#pragma mark Private methods
- (void)operateWithResponseObject:(id) responseObject
                            error:(NSError *) error
                       completion:(PMRequestCompletionBlock) completion
{
    if (!error) {
        [self setupResponse:responseObject];
        if (completion) {
            completion(self.lastError, self.response);
        }
    } else {
        if (completion) {
            self.lastError = error;
            completion(error, responseObject);
        }
    }
}

- (NSURL *)URLForRequest{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", PMSiteURL, [self paramsURL]]];
}

- (NSDictionary *)paramsForRequest{
    NSLog(@"Need to override in sub %s", __func__);
    abort();
}

- (NSString *)paramsURL{
    NSLog(@"Need to override in sub %s", __func__);
    abort();
}

- (void)setupResponse:(id) response{
    NSLog(@"Need to override in sub %s", __func__);
    abort();
}

@end
