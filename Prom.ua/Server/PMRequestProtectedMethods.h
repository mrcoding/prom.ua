//
//  PMRequestProtectedMethods.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PMRequestProtectedMethods <NSObject>

- (NSDictionary *)paramsForRequest;
- (NSString *)paramsURL;
- (void)setupResponse:(id) response;

@end

@interface PMRequest(ProtectedMethods) <PMRequestProtectedMethods>
@end