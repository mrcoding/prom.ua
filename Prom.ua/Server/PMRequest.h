//
//  PMRequest.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^PMRequestCompletionBlock)(NSError *error, id response);

@interface PMRequest : NSObject

+ (instancetype)request;

@property (nonatomic, strong) id        response;
@property (nonatomic, strong) NSError   *lastError;

- (void)sendRequestWithCompletion:(PMRequestCompletionBlock) completion;

@end
