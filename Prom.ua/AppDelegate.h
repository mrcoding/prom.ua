//
//  AppDelegate.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

