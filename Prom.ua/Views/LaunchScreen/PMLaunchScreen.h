//
//  PMLaunchScreen.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 7/14/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMLaunchScreen : UIViewController

+ (instancetype)controller;

- (void)hideScreen;

@end
