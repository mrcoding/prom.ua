//
//  PMLaunchScreen.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 7/14/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "PMLaunchScreen.h"

#import "PMKit.h"

@interface PMLaunchScreen ()
@property (weak, nonatomic) IBOutlet UIView *launchScreenView;

@end

@implementation PMLaunchScreen

#pragma mark -
#pragma mark Class Methods

+ (instancetype)controller {
    return [[self alloc] initWithNibName:NSStringFromClass([self class]) bundle:[NSBundle mainBundle]];
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark -
#pragma mark Public

- (void)hideScreen {
    UIView *view = self.view;
    
    [UIView animateWithDuration:0.2f animations:^{
        CGAffineTransform transform = CGAffineTransformIdentity;
        transform = CGAffineTransformScale(transform, 3.0f, 3.0f);
        view.transform = transform;
        view.alpha = 0;
    } completion:^(BOOL finished) {
        UIWindow *window = [MGBaseNavigation navigation].window;
        [UIApplication sharedApplication].delegate.window = window;
        [window makeKeyAndVisible];
    }];
}

@end
