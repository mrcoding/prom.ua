//
//  PMOrdersListViewController.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersListViewController.h"
#import "PMKit.h"
#import "PMOrderTableViewCell.h"
#import "PMOrderInfoViewController.h"
#import "PMNavPanelSearchView.h"

@interface PMOrdersListViewController () <UITableViewDelegate, UITableViewDataSource, PMOrdersManagerProtocol, PMOrdersFilterLogicProtocol>

@property (nonatomic, strong) NSArray<PMOrder *>        *orders;
@property (weak, nonatomic) IBOutlet UIImageView        *emptyView;
@property (weak, nonatomic) IBOutlet PMNavigationPanel  *navPanel;
@property (weak, nonatomic) IBOutlet UITableView        *tableView;
@property (nonatomic, strong) PMOrdersFilterLogic       *ordersFilter;
@property (nonatomic, strong) UIRefreshControl          *refreshControl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


@end

static CGFloat kPMOrdersListVCCellHeight = 70.f;

@implementation PMOrdersListViewController

#pragma mark -
#pragma mark Class methods
+ (instancetype)controller{
    return [UINib loadClass:[self class]];
}

#pragma mark -
#pragma mark Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavPanelElements];
    [self setupTableViewData];
    [self setupData];
}

- (void)dealloc{
    [[PMOrdersManager manager] removeObserver:self];
    [self.ordersFilter removeObserver:self];
}

#pragma mark -
#pragma mark Private methods
- (void)setupNavPanelElements{
    selfWeakCreate;
    [[self.navPanel removeAllLeftElements] addLogoButtonWithCompletion:^{
        
    }];
    
    [[self.navPanel removeAllRightElements] addSearchButtonWithCompletion:^{
        PMNavPanelSearchView *searchView = [PMNavPanelSearchView view];
        [searchView setFocused];
        [searchView onClose:^(id view) {
            [selfWeak.navPanel mainContainerVisible:YES animated:YES];
            selfWeak.ordersFilter.textFilter = @"";
            [selfWeak.ordersFilter updateFilter];
        }];
        [searchView onChanged:^(PMNavPanelSearchView *view) {
            selfWeak.ordersFilter.textFilter = view.text;
            [selfWeak.ordersFilter updateFilter];
        }];
        [selfWeak.navPanel showSecondView:searchView];
    }];
}

- (void)setupTableViewData{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.tableView addSubview:refreshControl];
}

- (void)setupData{
    [[PMOrdersManager manager] addObserver:self];
    [[PMOrdersManager manager] syncOrdersAndSaveToDB];
    self.ordersFilter = [PMOrdersFilterLogic filter];
    [self.ordersFilter addObserver:self];
    
    self.orders = [[PMOrdersManager manager] allOrders];
    [self reloadTableViewData];
    
    [self.indicator setHidesWhenStopped:YES];
    
    if (self.orders.count < 1) {
        [self.indicator startAnimating];
    }
}

- (void)reloadTableViewData{
    if (!self.refreshControl.refreshing ||
        !self.indicator.isAnimating)
    {
        self.emptyView.hidden = self.orders.count > 0;
        self.tableView.hidden = self.orders.count == 0;
    }
    
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Events
- (void)refresh:(UIRefreshControl *)refreshControl {
    [[PMOrdersManager manager] syncOrdersAndSaveToDB];
}

#pragma mark -
#pragma mark <UITableViewDataSource>
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kPMOrdersListVCCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PMOrderTableViewCell *cell = [tableView reusableTableCellForClass:[PMOrderTableViewCell class]];
    PMOrder *order = self.orders[indexPath.row];
    cell.order = order;
    
    [cell onTap:^(PMOrderTableViewCell *cell) {
        [[MGBaseNavigation navigation] showController:[PMOrderInfoViewController controllerWithOrderID:order.identifier]];
    }];
    
    return cell;
}

#pragma mark -
#pragma mark <UITableViewDataDelegate>
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CGAffineTransform transform = CGAffineTransformIdentity;
    transform = CGAffineTransformScale(transform, 0.f, 0.f);
    cell.transform = transform;
    cell.alpha = 0;
    
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.2];
        cell.layer.transform = CATransform3DIdentity;
        cell.alpha = 1;
        cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark <PMOrdersManagerProtocol>
- (void)ordersManagerDidChanged:(PMOrdersManager *)manager {
    if (self.refreshControl.refreshing) {
        [self.refreshControl endRefreshing];
    }
    
    if (self.indicator.isAnimating) {
        [self.indicator stopAnimating];
    }
    
    if (!manager.isLastSyncSuccess) {
        [MGAlert showAlertWithTitle:nil
                            message:@"Ошибка загрузки данных"
                        cancelTitle:@"Отмена"
                        secondTitle:@"Повтор" completion:^(MGAlert *alert) {
                            if (alert.selectedButtonIndex == 1) {
                                [[PMOrdersManager manager] syncOrdersAndSaveToDB];
                            }
                        }];
    }

    self.orders = [manager allOrders];
    [self reloadTableViewData];
}

#pragma mark -
#pragma mark <PMOrdersFilterLogicProtocol>
- (void)ordersFilterDidUpdate:(PMOrdersFilterLogic *)filter {
    self.orders = [filter filteredOrders];
    [self reloadTableViewData];
}

@end
