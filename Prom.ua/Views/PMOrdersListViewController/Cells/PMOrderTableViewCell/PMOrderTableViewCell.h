//
//  PMOrderTableViewCell.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMOrder;

@class PMOrderTableViewCell;

typedef void(^PMOrderTableViewCellTapBlock)(PMOrderTableViewCell * cell);

@interface PMOrderTableViewCell : UITableViewCell

@property (nonatomic, assign) PMOrder   *order;

- (void)onTap:(PMOrderTableViewCellTapBlock) block;

@end
