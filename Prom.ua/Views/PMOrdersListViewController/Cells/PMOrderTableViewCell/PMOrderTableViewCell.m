//
//  PMOrderTableViewCell.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrderTableViewCell.h"
#import "PMOrder.h"
#import "PMKit.h"
#import "PMConversion.h"
#import "PMOrdersManager.h"
#import "PMItem.h"

@interface PMOrderTableViewCell()


@property (weak, nonatomic) IBOutlet UILabel                *firstLineLabel;
@property (weak, nonatomic) IBOutlet UILabel                *secondLineLabel;
@property (weak, nonatomic) IBOutlet UILabel                *dateLabel;
@property (nonatomic, copy) PMOrderTableViewCellTapBlock    tapBlock;
@property (weak, nonatomic) IBOutlet PMTouchesView          *touchView;

@end

@implementation PMOrderTableViewCell

#pragma mark -
#pragma mark Lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self clearData];
    [self setup];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self clearData];
}

#pragma mark -
#pragma mark Accessors
- (void)setOrder:(PMOrder *)order {
    _order = order;
    [self updateUI];
}

#pragma mark -
#pragma mark Public methods
- (void)onTap:(PMOrderTableViewCellTapBlock)block {
    self.tapBlock = block;
}

#pragma mark -
#pragma mark Private methods
- (void)setup {
    selfWeakCreate;
    [self.touchView onTap:^(id view, NSSet *touches) {
        if (selfWeak.tapBlock) {
            selfWeak.tapBlock(selfWeak);
        }
    }];
}

- (void)updateUI{
    if (!self.order) return;
    PMOrder *order = self.order;
    if (order.orderState == kPMOrderStateNew) {
        self.firstLineLabel.text = [NSString stringWithFormat:@"№%li - %@", order.identifier, order.name];
    } else {
        self.firstLineLabel.text = [NSString stringWithFormat:@"%@ | №%li", [PMConversion stringFromOrderState:order.orderState], order.identifier];
    }
    
    self.dateLabel.text = order.date;
    
    PMItem *item = [[[PMOrdersManager manager] itemsByOrderID:order.identifier] firstObject];
    self.secondLineLabel.text = (item) ? [NSString stringWithFormat:@"%.02f %@ - %@", (item.quantity * item.price), item.currency, item.name] : @"-";
}

- (void)clearData {
    self.order = nil;
    self.firstLineLabel.text = @"";
    self.secondLineLabel.text = @"";
    self.dateLabel.text = @"";
}

@end
