//
//  PMOrdersListViewController.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrdersListViewController : UIViewController

+ (instancetype)controller;

@end
