//
//  PMOrderInfoView.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMOrder;

@interface PMOrderInfoView : UIView

+ (instancetype)view;

@property (nonatomic, copy) PMOrder *order;

@end
