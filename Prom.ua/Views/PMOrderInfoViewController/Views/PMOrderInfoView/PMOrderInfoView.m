//
//  PMOrderInfoView.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrderInfoView.h"
#import "PMKit.h"
#import "PMOrder.h"

@interface PMOrderInfoView()

@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;

@end

@implementation PMOrderInfoView

#pragma mark -
#pragma mark Class methods
+ (instancetype)view {
    return [UINib loadClass:[self class]];
}

#pragma mark -
#pragma mark Accessors
- (void)setOrder:(PMOrder *)order {
    _order = order;
    [self updateViewData];
}

#pragma mark -
#pragma mark Private methods
- (void)updateViewData{
    if (!self.order) return;
    PMOrder *order = self.order;
    self.idLabel.text = [self operateIfEmptyWithString:[NSString stringWithInt:order.identifier]];
    self.nameLabel.text = [self operateIfEmptyWithString:order.name];
    self.dateLabel.text = [self operateIfEmptyWithString:order.date];
    self.phoneLabel.text = [self operateIfEmptyWithString:order.phone];
    self.emailLabel.text = [self operateIfEmptyWithString:order.email];
    self.adressLabel.text = [self operateIfEmptyWithString:order.address];
}

- (NSString *)operateIfEmptyWithString:(NSString *) string{
    return (string.length > 0) ? string : @"-";
}

@end
