//
//  PMItemsTotalPriceTableViewCell.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMItemsTotalPriceTableViewCell.h"

@interface PMItemsTotalPriceTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end

@implementation PMItemsTotalPriceTableViewCell

#pragma mark -
#pragma mark Lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self clearData];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self clearData];
}

#pragma mark -
#pragma mark Accessors
- (void)setTotalPrice:(double)totalPrice{
    _totalPrice = totalPrice;
    self.priceLabel.text = [NSString stringWithFormat:@"%.02f %@", totalPrice, self.currency];
}

- (void)setCurrency:(NSString *) currency{
    _currency = currency;
    self.priceLabel.text = [NSString stringWithFormat:@"%.02f %@", self.totalPrice, currency];
}

#pragma mark -
#pragma mark Private methods
- (void)clearData{
    self.priceLabel.text = @"";
}


@end
