//
//  PMItemsTotalPriceTableViewCell.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMItemsTotalPriceTableViewCell : UITableViewCell

@property (nonatomic, assign) double    totalPrice;
@property (nonatomic, assign) NSString  *currency;

@end
