//
//  PMItemTableViewCell.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMItemTableViewCell.h"
#import "PMKit.h"
#import "PMItem.h"
#import "MGImageView.h"

@interface PMItemTableViewCell()

@property (weak, nonatomic) IBOutlet MGImageView    *image;
@property (weak, nonatomic) IBOutlet UILabel        *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel        *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel        *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel        *totalPriceLabel;


@end

@implementation PMItemTableViewCell

#pragma mark -
#pragma mark Lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self clearData];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self clearData];
}

#pragma mark -
#pragma mark Accessors
- (void)setItem:(PMItem *) item{
    _item = item;
    [self updateUI];
}

#pragma mark -
#pragma mark Private methods
- (void)clearData{
    self.image.imageURL = nil;
    self.nameLabel.text = nil;
    self.priceLabel.text = nil;
    self.quantityLabel.text = nil;
    self.totalPriceLabel.text = nil;
    self.item = nil;
}

- (void)updateUI{
    if (!self.item) return;
    PMItem *item = self.item;
    if (item.url.length > 0) {
        self.image.imageURL = [NSURL URLWithString:item.image];
    }
    self.nameLabel.text = item.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%.02f %@", item.price, item.currency.lowercaseString];
    self.quantityLabel.text = [NSString stringWithFormat:@"%.02f %@", item.quantity, @"шт."];
    double totalPrice = item.price * item.quantity;
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%.02f %@", totalPrice, item.currency.lowercaseString];
    
}

@end
