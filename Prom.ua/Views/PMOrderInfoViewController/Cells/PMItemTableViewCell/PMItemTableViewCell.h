//
//  PMItemTableViewCell.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMItem;

@interface PMItemTableViewCell : UITableViewCell

@property (nonatomic, copy) PMItem *item;

@end
