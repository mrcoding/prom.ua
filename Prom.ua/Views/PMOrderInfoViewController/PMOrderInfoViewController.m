//
//  PMOrderInfoViewController.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrderInfoViewController.h"
#import "PMKit.h"
#import "PMOrderInfoView.h"
#import "PMItemTableViewCell.h"
#import "PMItemsTotalPriceTableViewCell.h"

@interface PMOrderInfoViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) NSInteger                 orderID;
@property (nonatomic, assign) PMOrder                   *order;
@property (weak, nonatomic) IBOutlet PMNavigationPanel  *navPanel;
@property (weak, nonatomic) IBOutlet UIView             *infoView;
@property (nonatomic, strong) NSArray<PMItem *>         *items;
@property (weak, nonatomic) IBOutlet UITableView        *tableView;
@property (weak, nonatomic) IBOutlet UIPageControl      *pageControl;
@property (nonatomic, strong) PMOrderInfoView           *orderInfoView;

@end

static CGFloat kPMItemsListVCCellHeight = 80.f;

@implementation PMOrderInfoViewController

#pragma mark -
#pragma mark Class methods
+ (instancetype)controllerWithOrderID:(NSInteger)orderID {
    PMOrderInfoViewController *controller = [UINib loadClass:[self class]];
    controller.orderID = orderID;
    return controller;
}

#pragma mark -
#pragma mark Accessors
- (void)setOrderID:(NSInteger)orderID {
    _orderID = orderID;
    [self setupViews];
    self.navPanel.title = [NSString stringWithFormat:@"Заказ %li", orderID];
    PMOrdersManager *manager = [PMOrdersManager manager];
    self.items = [manager itemsByOrderID:self.orderID];
    if (self.items.count > 0) {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    } else self.tableView.hidden = YES;
}

#pragma mark -
#pragma mark Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavElements];
    [self setupViews];
    [self setupGestures];
}

#pragma mark -
#pragma mark Private methods
- (void)setupNavElements{
    [[self.navPanel removeAllLeftElements] addBackButton];
}

- (void)setupViews{
    PMOrder *order = nil;
    if (self.orderID > 0) {
        order = [PMOrder orderWithID:self.orderID];
        [order loadInfoFromDB];
        self.order = order;
    }
    
    if (!self.orderInfoView) {
        PMOrderInfoView *orderInfoView = [PMOrderInfoView view];
        self.orderInfoView = orderInfoView;
        orderInfoView.order = order;
        [self.infoView heightConstraintCreate:YES].constant = 158.f;
        [self.infoView addSubview:orderInfoView];
        [orderInfoView alignExpandToSuperview];
    } else {
        self.orderInfoView.order = order;
    }
    
    self.pageControl.numberOfPages = [[PMOrdersManager manager] allOrders].count;
    self.pageControl.currentPage = [self indexOfCurrentOrder];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)setupGestures {
    UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftEvent:)];
    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftGesture];
    
    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightGesture:)];
    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightGesture];
}

#pragma mark -
#pragma mark <UITableViewDataSource>
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kPMItemsListVCCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (self.items.count > 0) ? self.items.count + 1 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    UITableViewCell *cell = nil;
    if (row < self.items.count) {
        cell = [tableView reusableTableCellForClass:[PMItemTableViewCell class]];
        PMItem *item = self.items[indexPath.row];
        ((PMItemTableViewCell *)cell).item = item;
    } else {
        cell = [tableView reusableTableCellForClass:[PMItemsTotalPriceTableViewCell class]];
        ((PMItemsTotalPriceTableViewCell *)cell).totalPrice = self.order.priceUAH;
        ((PMItemsTotalPriceTableViewCell *)cell).currency = self.items[0].currency;
    }
    return cell;
}

#pragma mark -
#pragma mark <UITableViewDataDelegate>
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    CGAffineTransform transform = CGAffineTransformIdentity;
    transform = CGAffineTransformScale(transform, 0.f, 0.f);
    cell.transform = transform;
    cell.alpha = 0;
    
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.2];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

- (PMOrder *)getPrevOrder {
    NSInteger currentIndex = [self indexOfCurrentOrder];
    if ((currentIndex - 1) >= 0) {
        return [[[PMOrdersManager manager] allOrders] objectAtIndex:currentIndex - 1];
    }
    return self.order;
}

- (PMOrder *)getNextOrder {
    NSInteger currentIndex = [self indexOfCurrentOrder];
    NSArray<PMOrder *> *orders = [[PMOrdersManager manager] allOrders];
    if ((currentIndex + 1) < orders.count) {
        return [orders objectAtIndex:currentIndex + 1];
    }
    return self.order;
}

- (NSInteger)indexOfCurrentOrder{
    NSArray<PMOrder *> *orders = [[PMOrdersManager manager] allOrders];
    NSInteger indexOfCurrentObject = 0;
    for (int i = 0; i < orders.count; i++) {
        PMOrder *order = [orders objectAtIndex:i];
        if (order.identifier == self.order.identifier) {
            indexOfCurrentObject = i;
        }
    }
    return indexOfCurrentObject;
}

#pragma mark -
#pragma mark Events
- (void)swipeLeftEvent:(UIGestureRecognizer *)gesture {
    NSInteger nextOrderIdentifier = [self getNextOrder].identifier;
    if (self.orderID != nextOrderIdentifier) {
        self.orderID = nextOrderIdentifier;
    }
}

- (void)swipeRightGesture:(UIGestureRecognizer *)gesture {
    NSInteger prewOrderIdentifier = [self getPrevOrder].identifier;
    if (self.orderID != prewOrderIdentifier) {
        self.orderID = [self getPrevOrder].identifier;
    }
}

@end
