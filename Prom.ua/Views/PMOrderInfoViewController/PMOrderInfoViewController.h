//
//  PMOrderInfoViewController.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMOrderInfoViewController : UIViewController

+ (instancetype)controllerWithOrderID:(NSInteger)orderID;

@end
