//
//  PMRootViewController.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRootViewController : UIViewController

+ (instancetype)controllerWithLaunchOptions:(NSDictionary *) launchOptions;

@end
