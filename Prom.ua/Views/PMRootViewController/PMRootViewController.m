//
//  PMRootViewController.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMRootViewController.h"
#import "PMKit.h"
#import "PMOrdersListViewController.h"
#import "PMLaunchScreen.h"

@interface PMRootViewController ()

@property (nonatomic, strong) NSDictionary      *launchOptions;
@property (nonatomic, strong) NSURL             *launchURL;
@property (nonatomic, strong) NSDictionary      *remoteMessage; // push
@property (nonatomic, strong) PMLaunchScreen    *launchScreenController;

@end

@implementation PMRootViewController

#pragma mark -
#pragma mark Class methods
+ (instancetype)controllerWithLaunchOptions:(NSDictionary *) launchOptions{
    PMRootViewController *controller = [[self alloc] init];
    controller.launchOptions = launchOptions;
    controller.launchURL = launchOptions[UIApplicationLaunchOptionsURLKey];
    controller.remoteMessage = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    return controller;
}

#pragma mark -
#pragma mark Lifecycle
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupAndShowViewController];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.launchScreenController = [PMLaunchScreen controller];
    window.rootViewController = self.launchScreenController;
    [UIApplication sharedApplication].delegate.window = window;
    [window makeKeyAndVisible];
}

#pragma mark -
#pragma mark Private methods
- (void)setupAndShowViewController{
    dispatch_async(dispatch_get_main_queue(), ^{
        PMOrdersListViewController *controller = [PMOrdersListViewController controller];
        [[MGBaseNavigation navigation] showController:controller animation:NO];
        [self.launchScreenController hideScreen];
    });
}



@end
