//
//  PMDBItem.m
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//

#import "PMDBItem.h"
#import "PMKit.h"

@implementation PMDBItem

#pragma mark -
#pragma mark Class methods
+ (instancetype)fetchItemWithID:(NSInteger)identifier create:(BOOL)create {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %li", NSStringFromSelector(@selector(identifier)), identifier];
    
    PMDBItem *item = [self fetchFirstEntityWithPredicate:predicate];
    if (!item && create) {
        item = [PMDBItem createEntity];
        item.identifier = identifier;
    }
    return item;
}

+ (NSArray *)fetchItemsWithOrderID:(NSInteger)orderID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %li", NSStringFromSelector(@selector(orderID)), orderID];
    return [self fetchEntityWithPredicate:predicate];
}

+ (instancetype)fetchItemWithID:(NSInteger)identifier orderID:(NSInteger)orderID create:(BOOL)create {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %li && %K = %li",
                              NSStringFromSelector(@selector(identifier)), identifier,
                              NSStringFromSelector(@selector(orderID)), orderID];
    
    PMDBItem *item = [self fetchFirstEntityWithPredicate:predicate];
    if (!item && create) {
        item = [PMDBItem createEntity];
        item.identifier = identifier;
        item.orderID = orderID;
    }
    return item;
}

@end
