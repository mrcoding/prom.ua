//
//  PMDBItem+CoreDataProperties.m
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMDBItem+CoreDataProperties.h"
#import "PMKit.h"

@implementation PMDBItem (CoreDataProperties)

@dynamic identifier;
@dynamic currency;
@dynamic orderID;
@dynamic externalID;
@dynamic name;
@dynamic price;
@dynamic quantity;
@dynamic image;
@dynamic sku;
@dynamic url;

coreDataIntProperty(identifier, setIdentifier);
coreDataIntProperty(orderID, setOrderID);
coreDataIntProperty(externalID, setExternalID);
coreDataDoubleProperty(price, setPrice);
coreDataDoubleProperty(quantity, setQuantity);

@end
