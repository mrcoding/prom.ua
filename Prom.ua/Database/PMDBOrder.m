//
//  PMDBOrder.m
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//

#import "PMDBOrder.h"
#import "PMKit.h"

@implementation PMDBOrder

#pragma mark -
#pragma mark Class methods
+ (instancetype)fetchOrderWithID:(NSInteger) identifier create:(BOOL) create{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %li", NSStringFromSelector(@selector(identifier)), identifier];
    
    PMDBOrder *order = [self fetchFirstEntityWithPredicate:predicate];
    if (!order && create) {
        order = [PMDBOrder createEntity];
        order.identifier = identifier;
    }
    return order;
}

@end
