//
//  PMDBItem+CoreDataProperties.h
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMDBItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMDBItem (CoreDataProperties)

@property (nonatomic, assign) NSInteger             identifier;
@property (nonatomic, assign) NSInteger             externalID;
@property (nonatomic, assign) NSInteger             orderID;
@property (nonatomic, assign) NSString              *currency;
@property (nonatomic, assign) double                price;
@property (nonatomic, assign) double                quantity;
@property (nullable, nonatomic, retain) NSString    *image;
@property (nullable, nonatomic, retain) NSString    *name;
@property (nullable, nonatomic, retain) NSString    *sku;
@property (nullable, nonatomic, retain) NSString    *url;

@end

NS_ASSUME_NONNULL_END
