//
//  PMDBOrder.h
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMDBOrder : NSManagedObject

+ (instancetype)fetchOrderWithID:(NSInteger) identifier create:(BOOL) create;

@end

NS_ASSUME_NONNULL_END

#import "PMDBOrder+CoreDataProperties.h"
