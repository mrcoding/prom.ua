//
//  PMDBItem.h
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMDBItem : NSManagedObject

+ (instancetype)fetchItemWithID:(NSInteger)identifier create:(BOOL)create;
+ (instancetype)fetchItemWithID:(NSInteger)identifier orderID:(NSInteger)orderID create:(BOOL)create;
+ (NSArray *)fetchItemsWithOrderID:(NSInteger)orderID;

@end

NS_ASSUME_NONNULL_END

#import "PMDBItem+CoreDataProperties.h"
