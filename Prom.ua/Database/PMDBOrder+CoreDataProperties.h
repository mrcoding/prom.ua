//
//  PMDBOrder+CoreDataProperties.h
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMDBOrder.h"
#import "PMTypedefs.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMDBOrder (CoreDataProperties)

@property (nonatomic, assign) NSInteger             identifier;
@property (nullable, nonatomic, retain) NSString    *state;
@property (nullable, nonatomic, retain) NSString    *address;
@property (nullable, nonatomic, retain) NSString    *company;
@property (nullable, nonatomic, retain) NSString    *date;
@property (nullable, nonatomic, retain) NSString    *email;
@property (nullable, nonatomic, retain) NSString    *name;
@property (nullable, nonatomic, retain) NSString    *phone;
@property (nonatomic, assign) double                priceUAH;

@property (nonatomic, assign) PMOrderState          orderState;

@end

NS_ASSUME_NONNULL_END
