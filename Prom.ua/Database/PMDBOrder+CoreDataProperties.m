//
//  PMDBOrder+CoreDataProperties.m
//  
//
//  Created by Yaroslav Babalich on 5/28/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMDBOrder+CoreDataProperties.h"
#import "PMKit.h"
#import "PMConversion.h"

@implementation PMDBOrder (CoreDataProperties)

@dynamic identifier;
@dynamic state;
@dynamic address;
@dynamic company;
@dynamic date;
@dynamic email;
@dynamic name;
@dynamic phone;
@dynamic priceUAH;

coreDataIntProperty(identifier, setIdentifier);
coreDataDoubleProperty(priceUAH, setPriceUAH);

- (PMOrderState)orderState{
    return [PMConversion orderStateFromString:self.state];
}

- (void)setOrderState:(PMOrderState)orderState {
    self.state = [PMConversion stringFromOrderState:orderState];
}

@end
