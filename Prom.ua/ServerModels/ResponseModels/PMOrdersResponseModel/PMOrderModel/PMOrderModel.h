//
//  PMOrderModel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "PMItemModel.h"

@protocol PMOrderModel <NSObject>
@end

@protocol PMItemsDict <NSObject>
@end

@interface PMOrderModel : JSONModel

@property (nonatomic, copy) NSString<Optional>              *identifier;
@property (nonatomic, copy) NSString<Optional>              *state;
@property (nonatomic, copy) NSString<Optional>              *address;
@property (nonatomic, copy) NSString<Optional>              *company;
@property (nonatomic, copy) NSString<Optional>              *date;
@property (nonatomic, copy) NSString<Optional>              *email;
@property (nonatomic, copy) id<Optional>                    items;
@property (nonatomic, copy) NSString<Optional>              *name;
@property (nonatomic, copy) NSString<Optional>              *phone;
@property (nonatomic, copy) NSString<Optional>              *priceUAH;

@end

@interface PMItemsDict : JSONModel

@property (nonatomic, copy) id<Optional, PMItemModel>       item;

@end