//
//  PMItemModel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol PMItemModel <NSObject>
@end

@interface PMItemModel : JSONModel

@property (nonatomic, copy) NSString<Optional>    *identifier;
@property (nonatomic, copy) NSString<Optional>    *currency;
@property (nonatomic, copy) NSString<Optional>    *externalID;
@property (nonatomic, copy) NSString<Optional>    *name;
@property (nonatomic, copy) NSString<Optional>    *price;
@property (nonatomic, copy) NSString<Optional>    *quantity;
@property (nonatomic, copy) NSString<Optional>    *sku;
@property (nonatomic, copy) NSString<Optional>    *url;
@property (nonatomic, copy) NSString<Optional>    *image;

@end
