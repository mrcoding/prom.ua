//
//  PMItemModel.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMItemModel.h"

@implementation PMItemModel

#pragma mark -
#pragma mark Key mapper
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"_id" : @"identifier",
                                                       @"external_id" : @"externalID"
                                                       }];
}

@end
