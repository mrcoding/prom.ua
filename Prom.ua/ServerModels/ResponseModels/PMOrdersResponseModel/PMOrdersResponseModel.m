//
//  PMOrdersResponseModel.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersResponseModel.h"

@implementation PMOrdersResponseModel

#pragma mark -
#pragma mark Key mapper
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"__name" : @"name",
                                                       @"_date" : @"date"
                                                       }];
}

@end
