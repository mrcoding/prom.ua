//
//  PMOrdersResponseModel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/27/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "PMOrderModel.h"

@interface PMOrdersResponseModel : JSONModel

@property (nonatomic, copy) NSString<Optional>              *name;
@property (nonatomic, copy) NSString<Optional>              *date;
@property (nonatomic, copy) NSArray<Optional, PMOrderModel> *order;

@end
