//
//  PMOrdersRequestModel.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersRequestModel.h"

@implementation PMOrdersRequestModel

#pragma mark -
#pragma mark Key mapper
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"hash_tag" : @"hashTag"
                                                       }];
}

@end
