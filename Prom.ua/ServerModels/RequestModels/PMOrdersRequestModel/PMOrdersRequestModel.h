//
//  PMOrdersRequestModel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMRequestModel.h"

@interface PMOrdersRequestModel : PMRequestModel

@property (nonatomic, copy) NSString    *hashTag;

@end
