//
//  PMOrdersManager.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersManager.h"
#import "PMKit.h"
#import "PMOrdersRequest.h"
#import "PMSyncElement.h"

@interface PMOrdersManager()

@property (nonatomic, strong) PMSyncElement *syncElement;

@end

@implementation PMOrdersManager

#pragma mark -
#pragma mark Class methods
+ (instancetype)manager {
    static PMOrdersManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [PMOrdersManager new];
    });
    return manager;
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (instancetype)init {
    self = [super init];
    if (self) {
        self.syncElement = [PMSyncElement syncElement];
        self.isLastSyncSuccess = YES;
    }
    return self;
}


#pragma mark -
#pragma mark Public methods
- (void)addObserver:(NSObject<PMOrdersManagerProtocol> *)observer {
    [super addObserver:observer];
}

- (void)syncOrdersAndSaveToDB{
    PMOrdersRequest *request = [PMOrdersRequest request];
    [self.syncElement queueBlock:^(PMSyncElement *syncElement, BOOL afterWait) {
        [request sendRequestWithCompletion:^(NSError *error, PMOrdersResponseModel *response) {
            syncElement.finished = YES;
            if (!error) {
                [self saveOrdersToDB:response];
                [self notifyObserversWithChanges];
            } else {
                [self notifyObserversWithChanges];
            }
            self.isLastSyncSuccess = (!error);
        }];
    }];
}

- (NSArray<PMOrder *> *)allOrders{
    NSArray<PMDBOrder *> *ordersDB = [PMDBOrder fetchAllEntities];
    NSMutableArray<PMOrder *> *orders = [NSMutableArray new];
    for (PMDBOrder *orderDB in ordersDB) {
        PMOrder *order = [PMOrder orderWithDBOrder:orderDB];
        [orders addObject:order];
    }
    return orders;
}

- (NSArray<PMItem *> *)itemsByOrderID:(NSInteger) orderID{
    NSArray<PMDBItem *> *itemsDB = [PMDBItem fetchItemsWithOrderID:orderID];
    NSMutableArray<PMItem *> *items = [NSMutableArray new];
    for (PMDBItem *dbItem in itemsDB) {
        PMItem *item = [PMItem itemWithDBItem:dbItem];
        [items addObject:item];
    }
    return [items copy];
}

#pragma mark -
#pragma mark Private methods
- (void)saveOrdersToDB:(PMOrdersResponseModel *) response{
    NSManagedObjectContext *context = nil;
    for (PMOrderModel *order in response.order) {
        PMDBOrder *orderDB = [PMDBOrder fetchOrderWithID:order.identifier.integerValue create:YES];
        orderDB.state = order.state;
        orderDB.address = order.address;
        orderDB.company = order.company;
        orderDB.date = order.date;
        orderDB.email = order.email;
        orderDB.name = order.name;
        orderDB.phone = order.phone;
        orderDB.priceUAH = order.priceUAH.doubleValue;
        if ((PMItemsDict *)order.items) {
            [self saveToDBItems:(PMItemsDict *)order.items orderID:order.identifier.integerValue context:&context];
        }
        context = (context) ? : orderDB.managedObjectContext;
    }
    [context saveContextAndWait];
}

- (void)saveToDBItems:(PMItemsDict *) items
              orderID:(NSInteger) orderID
              context:( NSManagedObjectContext * __autoreleasing*) context
{
    NSArray<PMItemModel *> *itemsArray = @[];
    if ([items.item isKindOfClass:[NSArray class]]) {
        itemsArray = [NSArray arrayWithArray:(NSArray<PMItemModel *> *)items.item];
    } else {
        itemsArray = [NSArray arrayWithObject:(PMItemModel *)items.item];
    }
    
    if (itemsArray.count > 0) {
        for (PMItemModel *item in itemsArray) {
            PMDBItem *itemDB = [PMDBItem fetchItemWithID:item.identifier.integerValue orderID:orderID create:YES];
            itemDB.currency = item.currency;
            itemDB.externalID = item.externalID.integerValue;
            itemDB.name = item.name;
            itemDB.price = item.price.doubleValue;
            itemDB.quantity = item.quantity.doubleValue;
            itemDB.sku = item.sku;
            itemDB.url = item.url;
            itemDB.orderID = orderID;
            itemDB.image = item.image;
            *context = (*context) ? : itemDB.managedObjectContext;
        }
    }
}

- (void)notifyObserversWithChanges{
    [self notifyObserversOnMainThreadWithSelector:@selector(ordersManagerDidChanged:)
                                       withObject:self];
}

@end

