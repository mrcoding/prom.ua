//
//  PMOrdersManager.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMObservableObject.h"

@class PMOrdersManager;
@class PMOrder;
@class PMItem;

@protocol PMOrdersManagerProtocol <NSObject>

@required
- (void)ordersManagerDidChanged:(PMOrdersManager *) manager;

@end

@interface PMOrdersManager : PMObservableObject

+ (instancetype)manager;

@property (nonatomic, assign) BOOL isLastSyncSuccess;

- (NSArray<PMOrder *> *)allOrders;
- (NSArray<PMItem *> *)itemsByOrderID:(NSInteger) orderID;

- (void)syncOrdersAndSaveToDB;

- (void)addObserver:(NSObject<PMOrdersManagerProtocol> *) observer;

@end
