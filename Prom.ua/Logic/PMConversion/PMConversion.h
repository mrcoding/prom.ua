//
//  PMConversion.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/30/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMTypedefs.h"

@interface PMConversion : NSObject

//order
+ (PMOrderState)orderStateFromString:(NSString *)orderString;
+ (NSString *)stringFromOrderState:(PMOrderState)orderState;

@end
