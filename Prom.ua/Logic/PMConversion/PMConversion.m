//
//  PMConversion.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/30/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMConversion.h"

@implementation PMConversion

#pragma mark -
#pragma mark Class methods
+ (PMOrderState)orderStateFromString:(NSString *)orderString {
    return [self enumFromString:orderString dictionary:[self orderDict] unknownValue:kPMOrderStateUnknown];
}

+ (NSString *)stringFromOrderState:(PMOrderState)orderState{
    return [self stringFromEnum:orderState dictionary:[self orderDict] defaultValue:kPMOrderStateUnknown];
}

static NSDictionary * _orderDict = nil;
+ (NSDictionary *)orderDict{
    if (!_orderDict) {
        _orderDict = @{
                       @(kPMOrderStateUnknown) : @"UNKNOWN",
                       @(kPMOrderStateNew) : @"NEW",
                       @(kPMOrderStateAccepted) : @"ПРИНЯТЫЙ",
                       @(kPMOrderStateDeclined) : @"ACCEPTED",
                       @(kPMOrderStateDraft) : @"DRAFT",
                       @(kPMOrderStateClosed) : @"CLOSED"
                       };
    }
    return _orderDict;
}


#pragma mark -
#pragma mark Enum / String

+ (NSUInteger)enumFromString:(NSString *)string
                  dictionary:(NSDictionary *)dictionary
                unknownValue:(NSUInteger)unknownValue
{
    return (NSUInteger)[self enumLongFromString:string dictionary:dictionary unknownValue:unknownValue];
}

+ (NSString *)stringFromEnum:(NSUInteger)enumValue
                  dictionary:(NSDictionary *)dictionary
                defaultValue:(NSUInteger)defaultValue
{
    return [self stringFromEnumLong:enumValue dictionary:dictionary defaultValue:defaultValue];
}


+ (long long)enumLongFromString:(NSString *)string
                     dictionary:(NSDictionary *)dictionary
                   unknownValue:(long long)unknownValue
{
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    string = [[string uppercaseString] stringByTrimmingCharactersInSet:charSet];
    NSArray *keys = [dictionary allKeysForObject:string];
    if (keys.count > 0) {
        return [keys[0] intValue];
    }
    
    return unknownValue;
}

+ (NSString *)stringFromEnumLong:(long long)enumValue
                      dictionary:(NSDictionary *)dictionary
                    defaultValue:(long long)defaultValue
{
    NSString *result = dictionary[@(enumValue)];
    if (!result && defaultValue >= 0) {
        result = dictionary[@(defaultValue)];
    }
    return result;
}

@end
