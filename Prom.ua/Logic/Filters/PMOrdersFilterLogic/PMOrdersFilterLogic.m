//
//  PMOrdersFilterLogic.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrdersFilterLogic.h"
#import "PMKit.h"

@implementation PMOrdersFilterLogic

#pragma mark -
#pragma mark Class method
+ (instancetype)filter {
    return [self new];
}

#pragma mark -
#pragma mark Public methods
- (NSArray<PMOrder *> *)filteredOrders{
    return [self filteredOrders:[[PMOrdersManager manager] allOrders]];
}

- (void)addObserver:(NSObject<PMOrdersFilterLogicProtocol> *)observer {
    [super addObserver:observer];
}

- (void)updateFilter {
    [self notifyObserversOnMainThreadWithSelector:@selector(ordersFilterDidUpdate:)
                                       withObject:self];
}

#pragma mark -
#pragma mark Private methods

- (NSArray<PMOrder *> *)filteredOrders:(NSArray<PMOrder *> *)allOrders {
    if (self.textFilter.length == 0) {
        return allOrders;
    }
    
    NSArray *result = [allOrders filteredArrayWithBlock:^BOOL(PMOrder *order) {
        NSString *textFilter = [self.textFilter lowercaseString];
        
        if ([[NSString stringWithInt:order.identifier] containsString:textFilter]) { //filter by identifier
            return YES;
        }
        
        if ([[order.name lowercaseString] containsString:textFilter]) { //filter by name
            return YES;
        }
        
        if ([[order.phone lowercaseString] containsString:textFilter]) { //filter by phone
            return YES;
        }
        
        PMOrdersManager *manager = [PMOrdersManager manager];
        NSArray<PMItem *> *items = [manager itemsByOrderID:order.identifier];
        
        for (PMItem *item in items) {
            
            if ([[item.name lowercaseString] containsString:textFilter]) { //filter by item.name
                return YES;
            }
            
            if ([[item.sku lowercaseString] containsString:textFilter]) { //filter by item.sku
                return YES;
            }
            
        }
        
        return NO;
    }];
    
    return result;
}

@end
