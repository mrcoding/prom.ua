//
//  PMOrdersFilterLogic.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMObservableObject.h"

@class PMOrder;
@class PMOrdersFilterLogic;

@protocol PMOrdersFilterLogicProtocol <NSObject>
@required
- (void)ordersFilterDidUpdate:(PMOrdersFilterLogic *)filter;

@end

@interface PMOrdersFilterLogic : PMObservableObject

+ (instancetype)filter;

@property (nonatomic, strong) NSString  *textFilter;

- (NSArray<PMOrder *> *)filteredOrders;

- (void)updateFilter;

- (void)addObserver:(NSObject<PMOrdersFilterLogicProtocol> *)observer;
@end
