//
//  PMItem.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMDBItem;

@interface PMItem : NSObject

+ (instancetype)itemWithID:(NSInteger)identifier;
+ (instancetype)itemWithDBItem:(PMDBItem *)dbItem;

@property (nonatomic, assign) NSInteger     identifier;
@property (nonatomic, assign) NSInteger     externalID;
@property (nonatomic, assign) NSInteger     orderID;
@property (nonatomic, assign) NSString      *currency;
@property (nonatomic, assign) double        price;
@property (nonatomic, assign) double        quantity;
@property (nonatomic, copy) NSString        *image;
@property (nonatomic, copy) NSString        *name;
@property (nonatomic, copy) NSString        *sku;
@property (nonatomic, copy) NSString        *url;

- (void)loadInfoFromDB;

@end
