//
//  PMItem.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/29/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMItem.h"
#import "PMDBItem.h"

@implementation PMItem

#pragma mark -
#pragma mark Class methods
+ (instancetype)itemWithID:(NSInteger)identifier {
    PMItem *item = [PMItem new];
    item.identifier = identifier;
    return item;
}

+ (instancetype)itemWithDBItem:(PMDBItem *)dbItem {
    PMItem *item = nil;
    if (dbItem.identifier > 0) {
        item = [PMItem itemWithID:dbItem.identifier];
        [item loadInfoFromDBOrder:dbItem];
        
    }
    return item;
}

#pragma mark -
#pragma mark Public methods
- (void)loadInfoFromDB{
    if (self.identifier > 0 &&
        self.orderID > 0) {
        [self loadInfoFromDBOrder:[PMDBItem fetchItemWithID:self.identifier orderID:self.orderID create:NO]];
    }
}

#pragma mark -
#pragma mark Private methods
- (void)loadInfoFromDBOrder:(PMDBItem *)item {
    self.externalID = item.externalID;
    self.orderID = item.orderID;
    self.currency = item.currency;
    self.price = item.price;
    self.quantity = item.quantity;
    self.name = item.name;
    self.sku = item.sku;
    self.url = item.url;
    self.image = item.image;
}


@end
