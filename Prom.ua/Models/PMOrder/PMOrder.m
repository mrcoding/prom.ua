//
//  PMOrderModel.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMOrder.h"
#import "PMDBOrder.h"

@implementation PMOrder

#pragma mark -
#pragma mark Class methods
+ (instancetype)orderWithID:(NSUInteger) identifier{
    PMOrder *order = [PMOrder new];
    order.identifier = identifier;
    return order;
}

+ (instancetype)orderWithDBOrder:(PMDBOrder *) dbOrder{
    PMOrder *order = nil;
    if (dbOrder.identifier > 0) {
        order = [PMOrder orderWithID:dbOrder.identifier];
        [order loadInfoFromDBOrder:dbOrder];
        
    }
    return order;
}


#pragma mark -
#pragma mark Public methods
- (void)loadInfoFromDB{
    [self loadInfoFromDBOrder:[PMDBOrder fetchOrderWithID:self.identifier create:NO]];
}

#pragma mark -
#pragma mark Private methods
- (void)loadInfoFromDBOrder:(PMDBOrder *) dbOrder{
    self.state = dbOrder.state;
    self.address = dbOrder.address;
    self.company = dbOrder.company;
    self.date = dbOrder.date;
    self.email = dbOrder.email;
    self.name = dbOrder.name;
    self.phone = dbOrder.phone;
    self.priceUAH = dbOrder.priceUAH;
    self.orderState = dbOrder.orderState;
}

@end
