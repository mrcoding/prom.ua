//
//  PMOrderModel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMTypedefs.h"

@class PMDBOrder;


@interface PMOrder : NSObject

+ (instancetype)orderWithID:(NSUInteger) identifier;
+ (instancetype)orderWithDBOrder:(PMDBOrder *) dbOrder;

@property (nonatomic, assign) NSUInteger    identifier;
@property (nonatomic, retain) NSString      *state;
@property (nonatomic, retain) NSString      *address;
@property (nonatomic, retain) NSString      *company;
@property (nonatomic, retain) NSString      *date;
@property (nonatomic, retain) NSString      *email;
@property (nonatomic, retain) NSString      *name;
@property (nonatomic, retain) NSString      *phone;
@property (nonatomic, assign) double        priceUAH;
@property (nonatomic, assign) PMOrderState  orderState;

- (void)loadInfoFromDB;

@end
