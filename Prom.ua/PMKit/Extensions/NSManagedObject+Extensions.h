//
//  NSManagedObject+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 03.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <MagicalRecord/MagicalRecord.h>

@interface NSManagedObject (Extensions)

/**
 * Create new empty database entity.
 */
+ (instancetype)createEntity;

/**
 * Fetch all database objects of current class.
 */
+ (NSArray *)fetchAllEntities;

/**
 * Delete all database objects of current class.
 */
+ (void)deleteAllEntities;

/**
 * Fetch entity count.
 */
+ (NSInteger)entityCount;

/**
 * Fetch first entity from database with specified predicate.
 */
+ (instancetype)fetchFirstEntityWithPredicate:(NSPredicate *)predicate;

/**
 * Fetch entities from database with specified predicate.
 */
+ (NSArray *)fetchEntityWithPredicate:(NSPredicate *)predicate;

/**
 * Fetch entities from database with specified format and arguments.
 */
+ (NSArray *)fetchEntityWithFormat:(NSString *)format
                         arguments:(NSArray *)arguments;

+ (instancetype)fetchFirstOrderedByAttribute:(NSString *)attribute ascending:(BOOL)ascending;

- (void)saveContextAndWait;
- (void)saveContextInBackground;
- (void)deleteEntity;

- (void)setCustomValue:(id)value forKey:(NSString *)key;
- (id)customValueForKey:(NSString *)key;

@end
