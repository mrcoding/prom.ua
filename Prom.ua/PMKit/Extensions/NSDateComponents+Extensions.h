//
//  NSDateComponents+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LSOrderDateComponents){
    LSOrderDateComponentsEarlier,
    LSOrderDateComponentsLater,
    LSOrderDateComponentsEqual
};

@interface NSDateComponents (Extensions)

/**
 * Normalize date componenets
 */
- (void)normalizeDateComponentsFromCalendar:(NSCalendar *) calendar;

/**
 * Get date from current date components
 */
- (NSDate *)dateFromCalendar:(NSCalendar *) calendar;

/**
 * Is equal 2 date components
 */
- (BOOL)isEqualToDateComponents:(NSDateComponents *) dateComponents;

/**
 * Add day to current day components
 */
- (void)addDaysCount:(NSInteger) daysCount;

/**
 * Add month to current day components
 */
- (void)addMonthCount:(NSInteger) monthCount;

/**
 * Compare 2 date components
 */
- (BOOL)compareWithAnother:(NSDateComponents *) dateComponents
                 withOrder:(LSOrderDateComponents) order;


- (BOOL)betweenFirstDateComponents:(NSDateComponents *) firstDateComponents
           andSecondDateComponents:(NSDateComponents *) secondDateComponents;

@end
