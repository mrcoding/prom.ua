//
//  UICollectionView+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 21.09.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (Extensions)


- (id)dequeueCellWithClass:(Class) cellClass forIndexPath:(NSIndexPath *) indexPath;
- (__kindof UICollectionReusableView *)dequeueHeaderViewWithIdentifier:(NSString *) identifier
                                                          forIndexPath:(NSIndexPath *) indexPath;

@end
