//
//  NSBundle+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 06.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Extensions)

+ (id)loadNibWithClass:(Class) nibClass;
+ (id)loadNibWithClass:(Class) nibClass owner:(id) owner;
+ (id)loadNibWithClass:(Class) nibClass owner:(id) owner options:(NSDictionary *) options;

@end
