//
//  NSThread+Extensions.m
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/22/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSThread+Extensions.h"

@implementation NSThread (Extensions)

+ (void)performSynchonouslyOnMainThread:(void (^)()) block{
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self performSynchonouslyOnMainThread:block];
        });
        
        return;
    }
    
    block();
}

@end
