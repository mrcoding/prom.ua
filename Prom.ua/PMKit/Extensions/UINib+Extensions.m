//
//  UINib+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 11.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "UINib+Extensions.h"

@implementation UINib (Extensions)

+ (id)loadClass:(Class) class{
    return [self loadWithNibName:NSStringFromClass(class)
                        andOwner:class];
}

+ (id)loadWithNibName:(NSString *) nibName
             andOwner:(Class) owner
{
    UINib *nib = [self nibWithNibName:nibName bundle:[NSBundle mainBundle]];
    return [nib objectWithOwner:owner];
}

- (id)objectWithOwner:(Class) owner{
    NSArray *objects = [self instantiateWithOwner:owner
                                          options:nil];
    
    id nib = nil;
    for (id object in objects) {
        if ([object isKindOfClass:owner]){
            nib = object;
        }
    }
    
    return nib;
}

@end
