//
//  NSDate+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)

- (NSDateComponents *)getComponentsFromCalendar:(NSCalendar *) calendar;

- (NSDate *)dateWithDate:(NSDate *) anotherDate
               orderedBy:(NSComparisonResult) orderType;

@end
