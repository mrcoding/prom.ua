//
//  UIDevice+Extensions.m
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/22/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "UIDevice+Extensions.h"

@implementation UIDevice (Extensions)

+ (UIInterfaceOrientation)interfaceOrientation{
    // UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) // not interface orientation
    return [[UIApplication sharedApplication] statusBarOrientation];
}

+ (BOOL)isPortraitInterfaceOrientation{
    UIInterfaceOrientation orientation = [self interfaceOrientation];
    return orientation == UIDeviceOrientationUnknown
    || orientation == UIDeviceOrientationPortrait
    || orientation == UIDeviceOrientationPortraitUpsideDown;
}

+ (BOOL)isLandscapeInterfaceOrientation{
    return ![self isPortraitInterfaceOrientation];
}

+ (void)changeDeviceOrientationToOrientation:(UIDeviceOrientation) orientation{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        [[UIDevice currentDevice] setValue:@(orientation) forKey:@"orientation"];
    }
}

@end
