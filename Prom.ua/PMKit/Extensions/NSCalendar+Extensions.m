//
//  NSCalendar+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "NSCalendar+Extensions.h"
#import "NSDateComponents+Extensions.h"

@implementation NSCalendar (Extensions)

- (NSRange)rangeOfDaysForDateComponents:(NSDateComponents *) dateComponents{
    return [self rangeOfUnit:NSCalendarUnitDay
                      inUnit:NSCalendarUnitMonth
                     forDate:[self dateFromComponents:dateComponents]];
}

- (NSInteger)daysFromDateComp:(NSDateComponents *) fromDateComponents
                   toDateComp:(NSDateComponents *) toDateComponents
{
    NSDateComponents *daysComponents = [self components:NSCalendarUnitDay
                                               fromDate:[fromDateComponents dateFromCalendar:self]
                                                 toDate:[toDateComponents dateFromCalendar:self]
                                                options:NSCalendarWrapComponents];
    return [daysComponents day];
}

@end
