//
//  NSBundle+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 06.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSBundle+Extensions.h"
#import <UIKit/UINibLoading.h>

@implementation NSBundle (Extensions)

+ (id)loadNibWithClass:(Class) nibClass{
    return [self loadNibWithClass:nibClass
                            owner:nil];
}

+ (id)loadNibWithClass:(Class) nibClass owner:(id) owner{
    return [self loadNibWithClass:nibClass
                            owner:owner
                          options:nil];
}

+ (id)loadNibWithClass:(Class) nibClass owner:(id) owner options:(NSDictionary *) options{
    id result = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(nibClass)
                                                             owner:owner
                                                           options:options];
    for (id currentObject in topLevelObjects) {
        if([currentObject isMemberOfClass:nibClass]) {
            result = currentObject;
            break;
        }
    }
    
    return  result;
}

@end
