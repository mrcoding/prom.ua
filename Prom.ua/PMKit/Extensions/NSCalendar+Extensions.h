//
//  NSCalendar+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (Extensions)

- (NSRange)rangeOfDaysForDateComponents:(NSDateComponents *) dateComponents;
- (NSInteger)daysFromDateComp:(NSDateComponents *) fromDateComponents
                   toDateComp:(NSDateComponents *) toDateComponents;

@end
