//
//  UITableView+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 06.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "UITableView+Extensions.h"
#import "NSBundle+Extensions.h"

@implementation UITableView (Extensions)

- (id)reusableTableCellForClass:(Class) viewClass{
    NSString *cellIdentifier = NSStringFromClass(viewClass);
    UITableViewCell *cell = [self dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:cellIdentifier bundle:[NSBundle mainBundle]];
        [self registerNib:nib forCellReuseIdentifier:cellIdentifier];
        
        cell = [self dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = (id)[NSBundle loadNibWithClass:viewClass];
        }
    }
    
    return cell;
}

@end
