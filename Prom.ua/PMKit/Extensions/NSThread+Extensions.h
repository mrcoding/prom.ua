//
//  NSThread+Extensions.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/22/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSThread (Extensions)

+ (void)performSynchonouslyOnMainThread:(void (^)()) block;

@end
