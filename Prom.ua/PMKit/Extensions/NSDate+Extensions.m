//
//  NSDate+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "NSDate+Extensions.h"
#import "NSDateComponents+Extensions.h"

@implementation NSDate (Extensions)

- (NSDateComponents *)getComponentsFromCalendar:(NSCalendar *) calendar{
    
    NSLog(@"self = %@", self.description);
    
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay |
                                                         NSCalendarUnitMonth |
                                                         NSCalendarUnitYear |
                                                         NSCalendarUnitWeekday |
                                                         NSCalendarUnitWeekdayOrdinal |
                                                         NSCalendarUnitEra
                                                         )
                                               fromDate:self];
    [components normalizeDateComponentsFromCalendar:calendar];
    return components;
}

- (NSDate *)dateWithDate:(NSDate *) anotherDate
               orderedBy:(NSComparisonResult) orderType
{
    
    if (orderType == NSOrderedDescending){
        if ([self compare:anotherDate] == NSOrderedDescending) {
            return self;
        } else if([self compare:anotherDate] == NSOrderedDescending) {
            return anotherDate;
        }
    } else if (orderType == NSOrderedAscending){
        if ([self compare:anotherDate] == NSOrderedAscending) {
            return self;
        } else if([self compare:anotherDate] == NSOrderedAscending) {
            return anotherDate;
        }
    } else if (orderType == NSOrderedSame) {
        if ([self compare:anotherDate] == NSOrderedSame) {
            return self;
        }
    }
    
    return self;
}

@end
