//
//  NSObject+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 16.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extensions)

+ (NSArray *)allPropertyNames;
- (NSString *)allPropertiesValuesString;

@end
