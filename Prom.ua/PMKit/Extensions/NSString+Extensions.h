//
//  NSString+Extensions.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/22/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

+ (NSString *)stringWithInt:(NSInteger) intValue;
- (NSString *)MD5String;

@end
