//
//  UIDevice+Extensions.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/22/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Extensions)

+ (UIInterfaceOrientation)interfaceOrientation;
+ (BOOL)isPortraitInterfaceOrientation;
+ (BOOL)isLandscapeInterfaceOrientation;
+ (void)changeDeviceOrientationToOrientation:(UIDeviceOrientation) orientation;

@end
