//
//  NSIndexPath+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 17.09.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (Extensions)

- (BOOL)betweenFirstIndexPath:(NSIndexPath *) firstIndexPath
           andSecondIndexPath:(NSIndexPath *) secondIndexPath;

@end
