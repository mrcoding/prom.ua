//
//  UICollectionView+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 21.09.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "UICollectionView+Extensions.h"
#import "NSBundle+Extensions.h"

@implementation UICollectionView (Extensions)

- (id)dequeueCellWithClass:(Class) cellClass forIndexPath:(NSIndexPath *) indexPath{
    NSString *cellIdentifier = NSStringFromClass(cellClass);
    UICollectionViewCell *cell = [self dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        UINib *nib = [UINib nibWithNibName:cellIdentifier bundle:[NSBundle mainBundle]];
        [self registerNib:nib forCellWithReuseIdentifier:cellIdentifier];
        
        cell = [self dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (!cell) {
            cell = (id)[NSBundle loadNibWithClass:cellClass];
        }
    }
    return cell;
}

- (__kindof UICollectionReusableView *)dequeueHeaderViewWithIdentifier:(NSString *) identifier
                                                          forIndexPath:(NSIndexPath *) indexPath
{
    return [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                    withReuseIdentifier:identifier
                                           forIndexPath:indexPath];
}

@end
