//
//  NSDateComponents+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 15.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "NSDateComponents+Extensions.h"
#import "NSDate+Extensions.h"

@implementation NSDateComponents (Extensions)

- (void)normalizeDateComponentsFromCalendar:(NSCalendar *) calendar{
    NSDate *dateFromCalendar = [self dateFromCalendar:calendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay |
                                                         NSCalendarUnitMonth |
                                                         NSCalendarUnitYear |
                                                         NSCalendarUnitWeekday|
                                                         NSCalendarUnitHour |
                                                         NSCalendarUnitMinute |
                                                         NSCalendarUnitSecond //|
                                                        // NSCalendarUnitEra
                                                         )
                                               fromDate:dateFromCalendar];
    self.day = components.day;
    self.month = components.month;
    self.year = components.year;
    self.weekday = components.weekday;
    self.hour = components.hour;
    self.minute = components.minute;
    self.second = components.second;
}

- (NSDate *)dateFromCalendar:(NSCalendar *) calendar{
    NSDate *dateFromCalendar = [calendar dateFromComponents:self];
    return dateFromCalendar;
}

- (BOOL)isEqualToDateComponents:(NSDateComponents *) dateComponents{
    if (![dateComponents isKindOfClass:[NSDateComponents class]]) return NO;
    
    if (self.day != dateComponents.day ||
        self.month != dateComponents.month ||
        self.year != dateComponents.year)
    {
        return NO;
    }
    
    return YES;
}

- (void)addDaysCount:(NSInteger) daysCount{
    self.day += daysCount;
    [self normalizeDateComponentsFromCalendar:[NSCalendar currentCalendar]];
}

- (void)addMonthCount:(NSInteger) monthCount{
    self.month += monthCount;
    [self normalizeDateComponentsFromCalendar:[NSCalendar currentCalendar]];
}

- (BOOL)compareWithAnother:(NSDateComponents *) dateComponents
                               withOrder:(LSOrderDateComponents) order
{
    
    NSDate *currentDate = [self dateFromCalendar:[NSCalendar currentCalendar]];
    NSDate *secondDate = [dateComponents dateFromCalendar:[NSCalendar currentCalendar]];
    
    if (order == LSOrderDateComponentsEarlier) {
        if ([currentDate compare:secondDate] == NSOrderedDescending) {
            return YES;
        }
    } else if (order == LSOrderDateComponentsLater) {
        if ([currentDate compare:secondDate] == NSOrderedAscending) {
            return YES;
        }
    } else if (order == LSOrderDateComponentsEqual) {
        if ([currentDate compare:secondDate] == NSOrderedSame) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)betweenFirstDateComponents:(NSDateComponents *) firstDateComponents
           andSecondDateComponents:(NSDateComponents *) secondDateComponents
{
    NSDate *currentDate = [self dateFromCalendar:[NSCalendar currentCalendar]];
    NSDate *firstDate = [firstDateComponents dateFromCalendar:[NSCalendar currentCalendar]];
    NSDate *secondDate = [secondDateComponents dateFromCalendar:[NSCalendar currentCalendar]];
    
    if ([currentDate compare:firstDate] == NSOrderedAscending) {
        return NO;
    }
    
    if ([currentDate compare:secondDate] == NSOrderedDescending) {
        return NO;
    }
    
    return YES;
}

@end
