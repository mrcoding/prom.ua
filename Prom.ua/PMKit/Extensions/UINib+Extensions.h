//
//  UINib+Extensions.h
//  Calendar
//
//  Created by Yaroslav Babalich on 11.09.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINib (Extensions)

+ (id)loadClass:(Class) class;

@end
