//
//  UIView+Extenstions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 07.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extenstions)

/**
 * Remove all subviews
 */
- (void)removeAllSubviews;

/**
 * All constraint methods
 */

- (void)alignExpandToSuperview;
- (NSLayoutConstraint *)aspectRatioHeightToWidthWithScale:(CGFloat) scale;
- (NSLayoutConstraint *)aspectRatioWidthToHeightWithScale:(CGFloat) scale;
- (NSLayoutConstraint *)alignHeightToView:(UIView *) view scale:(CGFloat) scale padding:(CGFloat) padding;
- (NSLayoutConstraint *)alignWidthToView:(UIView *) view scale:(CGFloat) scale padding:(CGFloat) padding;
- (NSLayoutConstraint *)alignCenterXToView:(UIView *) view;
- (NSLayoutConstraint *)alignCenterYToView:(UIView *) view;
- (NSLayoutConstraint *)alignTopToView:(UIView *) view toTop:(BOOL) toTop padding:(CGFloat) padding;
- (NSLayoutConstraint *)alignLeftToView:(UIView *) view toLeft:(BOOL) toLeft padding:(CGFloat) padding;
- (NSLayoutConstraint *)alignRightToView:(UIView *) view toRight:(BOOL) toRight padding:(CGFloat) padding;
- (NSLayoutConstraint *)alignBottomToView:(UIView *) view toBottom:(BOOL) toBottom padding:(CGFloat) padding;

- (NSLayoutConstraint *)widthConstraintCreate:(BOOL)needToCreate;
- (NSLayoutConstraint *)heightConstraintCreate:(BOOL)needToCreate;
- (NSLayoutConstraint *)widthConstraintCreate:(BOOL)needToCreate priority:(UILayoutPriority)priority;
- (NSLayoutConstraint *)heightConstraintCreate:(BOOL)needToCreate priority:(UILayoutPriority)priority;
- (NSLayoutConstraint *)constraintToSelfWithAttribute:(NSLayoutAttribute) attribute;

- (NSLayoutConstraint *)alignToView:(UIView *) view
                               attr:(NSLayoutAttribute) attr
                             toAttr:(NSLayoutAttribute) toAttr
                            padding:(CGFloat) padding
                         multiplier:(CGFloat) multiplier;

- (NSLayoutConstraint *)alignFromAttr:(NSLayoutAttribute) attr1
                               toView:(UIView *) view
                                 attr:(NSLayoutAttribute) attr2
                             relation:(NSLayoutRelation) relation
                              padding:(CGFloat) padding
                           multiplier:(CGFloat) multiplier;

@end
