//
//  NSArray+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 31.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extensions)

- (NSArray *)filteredArrayWithBlock:(BOOL (^)(id)) block;

@end
