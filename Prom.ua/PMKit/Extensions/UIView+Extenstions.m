//
//  UIView+Extenstions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 07.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "UIView+Extenstions.h"

@implementation UIView (Extenstions)

- (void)removeAllSubviews{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
}

- (void)alignExpandToSuperview{
    UIView *currentView = self;
    UIView *superview = self.superview;
    if (!superview) {
        return;
    }
    
    currentView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = NSDictionaryOfVariableBindings(currentView);
    [superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[currentView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
    [superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[currentView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
}

- (NSLayoutConstraint *)aspectRatioHeightToWidthWithScale:(CGFloat) scale
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self
                                                                  attribute:NSLayoutAttributeWidth
                                                                 multiplier: scale / 1.f
                                                                   constant:0.f];
    constraint.priority = UILayoutPriorityRequired - 1;
    [self addConstraint:constraint];
    return constraint;
}

- (NSLayoutConstraint *)aspectRatioWidthToHeightWithScale:(CGFloat) scale
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                 multiplier: scale / 1.f
                                                                   constant:0.f];
    constraint.priority = UILayoutPriorityRequired - 1;
    [self addConstraint:constraint];
    return constraint;
}

- (NSLayoutConstraint *)alignHeightToView:(UIView *) view scale:(CGFloat) scale padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeHeight
                      toAttr:NSLayoutAttributeHeight padding:padding multiplier:scale];
}

- (NSLayoutConstraint *)alignWidthToView:(UIView *) view scale:(CGFloat) scale padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeWidth
                      toAttr:NSLayoutAttributeWidth padding:padding multiplier:scale];
}

- (NSLayoutConstraint *)alignCenterXToView:(UIView *) view{
    return [self alignToView:view attr:NSLayoutAttributeCenterX
                      toAttr:NSLayoutAttributeCenterX padding:0 multiplier:1];
}

- (NSLayoutConstraint *)alignCenterYToView:(UIView *) view{
    return [self alignToView:view attr:NSLayoutAttributeCenterY
                      toAttr:NSLayoutAttributeCenterY padding:0 multiplier:1];
}

- (NSLayoutConstraint *)alignTopToView:(UIView *) view toTop:(BOOL) toTop padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeTop
                      toAttr:toTop ? NSLayoutAttributeTop : NSLayoutAttributeBottom
                     padding:padding multiplier:1];
}

- (NSLayoutConstraint *)alignLeftToView:(UIView *) view toLeft:(BOOL) toLeft padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeLeft
                      toAttr:toLeft ? NSLayoutAttributeLeft : NSLayoutAttributeRight
                     padding:padding multiplier:1];
}

- (NSLayoutConstraint *)alignRightToView:(UIView *) view toRight:(BOOL) toRight padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeRight
                      toAttr:toRight ? NSLayoutAttributeRight : NSLayoutAttributeLeft
                     padding:toRight ? -padding : padding
                  multiplier:1];
}

- (NSLayoutConstraint *)alignBottomToView:(UIView *) view toBottom:(BOOL) toBottom padding:(CGFloat) padding{
    return [self alignToView:view attr:NSLayoutAttributeBottom
                      toAttr:toBottom ? NSLayoutAttributeBottom : NSLayoutAttributeTop
                     padding:toBottom ? -padding : padding
                  multiplier:1];
}

- (NSLayoutConstraint *)alignToView:(UIView *) view
                               attr:(NSLayoutAttribute) attr
                             toAttr:(NSLayoutAttribute) toAttr
                            padding:(CGFloat) padding
                         multiplier:(CGFloat) multiplier
{
    return [self alignFromAttr:attr toView:view attr:toAttr relation:NSLayoutRelationEqual
                       padding:padding multiplier:multiplier];
}


- (NSLayoutConstraint *)alignFromAttr:(NSLayoutAttribute) attr1
                               toView:(UIView *) view
                                 attr:(NSLayoutAttribute) attr2
                             relation:(NSLayoutRelation) relation
                              padding:(CGFloat) padding
                           multiplier:(CGFloat) multiplier
{
    NSLayoutConstraint *constraint = nil;
    if (view) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                      attribute:attr1
                                                                      relatedBy:relation
                                                                         toItem:view
                                                                      attribute:attr2
                                                                     multiplier:multiplier
                                                                       constant:padding];
        constraint.priority = UILayoutPriorityRequired - 1;
        [self.superview addConstraint:constraint];
    }
    
    return constraint;
}

- (NSLayoutConstraint *)widthConstraintCreate:(BOOL)needToCreate {
    return [self widthConstraintCreate:needToCreate priority:UILayoutPriorityRequired];
}

- (NSLayoutConstraint *)widthConstraintCreate:(BOOL) needToCreate priority:(UILayoutPriority) priority{
    // try to find constraint
    NSLayoutConstraint *constraint = [self constraintToSelfWithAttribute:NSLayoutAttributeWidth];
    
    if (needToCreate && constraint.priority != priority) {
        [self removeConstraint:constraint];
        constraint = nil;
    }
    
    // create constraint
    if (!constraint && needToCreate) {
        constraint = [NSLayoutConstraint constraintWithItem:self
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1.0
                                                   constant:self.frame.size.width];
        constraint.priority = priority;
        [self addConstraint:constraint];
    }
    
    return constraint;
}

- (NSLayoutConstraint *)heightConstraintCreate:(BOOL)needToCreate{
    return [self heightConstraintCreate:needToCreate priority:UILayoutPriorityRequired];
}

- (NSLayoutConstraint *)heightConstraintCreate:(BOOL)needToCreate priority:(UILayoutPriority)priority {
    // try to find constraint
    NSLayoutConstraint *constraint = [self constraintToSelfWithAttribute:NSLayoutAttributeHeight];
    
    if (needToCreate && constraint.priority != priority) {
        [self removeConstraint:constraint];
        constraint = nil;
    }
    
    // create constraint
    if (!constraint && needToCreate) {
        constraint = [NSLayoutConstraint constraintWithItem:self
                                                  attribute:NSLayoutAttributeHeight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1.0
                                                   constant:self.frame.size.height];
        constraint.priority = priority;
        [self addConstraint:constraint];
    }
    
    return constraint;
}


- (NSLayoutConstraint *)constraintToSelfWithAttribute:(NSLayoutAttribute) attribute{
    NSLayoutConstraint *constraint = nil;
    for (NSLayoutConstraint *constraintTmp in self.constraints) {
        if ((constraintTmp.firstAttribute == attribute
             && constraintTmp.firstItem == self)
            || ((constraintTmp.secondAttribute == attribute
                 && constraintTmp.secondItem == self)))
        {
            constraint = constraintTmp;
            break;
        }
    }
    
    return constraint;
}

@end
