//
//  NSMutableArray+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 31.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSMutableArray+Extensions.h"

@implementation NSMutableArray (Extensions)

- (void)exchangeRandomElements{
    NSUInteger count = self.count;
    if (count == 2) {
        [self exchangeObjectAtIndex:0 withObjectAtIndex:1];
    } else {
        for (uint i = 0; i < count - 1; i++) {
            int nElements = (uint)count - i;
            int randomElement = arc4random_uniform(nElements) + i;
            [self exchangeObjectAtIndex:i withObjectAtIndex:randomElement];
        }
    }
}

@end
