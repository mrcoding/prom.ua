//
//  NSManagedObject+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 03.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSManagedObject+Extensions.h"

@implementation NSManagedObject (Extensions)

#pragma mark -
#pragma mark Class Methods

+ (instancetype)createEntity {
    return [self MR_createEntity];
}

+ (NSArray *)fetchAllEntities {
    return [[self MR_findAll] copy];
}

+ (void)deleteAllEntities {
    for (NSManagedObject *object in [self fetchAllEntities]) {
        [object deleteEntity];
    }
}

+ (NSInteger)entityCount{
    return [self MR_countOfEntities];
}

+ (instancetype)fetchFirstEntityWithPredicate:(NSPredicate *)predicate {
    return [self MR_findFirstWithPredicate:predicate];
}

+ (NSArray *)fetchEntityWithPredicate:(NSPredicate *)predicate {
    return [[self MR_findAllWithPredicate:predicate] copy];
}

+ (NSArray *)fetchEntityWithFormat:(NSString *)format
                         arguments:(NSArray *)arguments
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:format
                                                argumentArray:arguments];
    return [[self fetchEntityWithPredicate:predicate] copy];
}

+ (instancetype)fetchFirstOrderedByAttribute:(NSString *)attribute ascending:(BOOL)ascending {
    return [self MR_findFirstOrderedByAttribute:attribute ascending:ascending];
}

#pragma mark -
#pragma mark Public

- (void)setCustomValue:(id)value forKey:(NSString *)key {
    [self willChangeValueForKey:key];
    [self setPrimitiveValue:value forKey:key];
    [self didChangeValueForKey:key];
}

- (id)customValueForKey:(NSString *)key {
    [self willAccessValueForKey:key];
    id result = [self primitiveValueForKey:key];
    [self didAccessValueForKey:key];
    
    return result;
}

- (void)saveContextAndWait {
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (void)saveContextInBackground {
    [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:nil];
}

- (void)deleteEntity {
    [self MR_deleteEntity];
}

@end
