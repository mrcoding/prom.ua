//
//  NSManagedObjectContext+Extensions.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Extensions)

- (void)saveContextAndWait;
- (void)saveContextInBackground;

@end
