//
//  NSMutableArray+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 31.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Extensions)

/**
 * Method for mix all elements in array.
 */
- (void)exchangeRandomElements;

@end
