//
//  NSObject+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 16.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSObject+Extensions.h"
#import <objc/runtime.h>

@implementation NSObject (Extensions)

+ (NSArray *)allPropertyNames{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableArray *properiesNamesArray = [NSMutableArray new];
    
    for (int i = 0; i < count; i++){
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [properiesNamesArray addObject:name];
    }
    
    return [properiesNamesArray copy];
}

- (NSString *)allPropertiesValuesString{
    NSArray *allProperyNames = [[self class] allPropertyNames];
    NSMutableString *string = [NSMutableString new];
    [string appendFormat:@"\nCLASS NAME -> %@ \n", NSStringFromClass([self class])];
    for (NSString *property in allProperyNames) {
        id value = [self valueForKey:property];
        [string appendFormat:@"%@ -> %@ \n", property, value];
    }
    return string;
}

@end
