//
//  NSArray+Extensions.m
//  Learn Words
//
//  Created by Yaroslav Babalich on 31.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (Extensions)

- (NSArray *)filteredArrayWithBlock:(BOOL (^)(id)) block{
    NSArray *result = [self filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings) {
        return block(object);
    }]];
    return result;
}

@end
