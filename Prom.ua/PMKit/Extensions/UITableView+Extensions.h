//
//  UITableView+Extensions.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 06.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extensions)

- (id)reusableTableCellForClass:(Class) viewClass;

@end
