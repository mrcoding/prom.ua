//
//  NSManagedObjectContext+Extensions.m
//  Prom.ua
//
//  Created by Yaroslav Babalich on 5/28/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "NSManagedObjectContext+Extensions.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation NSManagedObjectContext (Extensions)

- (void)saveContextAndWait {
    [self MR_saveToPersistentStoreAndWait];
}

- (void)saveContextInBackground {
    [self MR_saveToPersistentStoreWithCompletion:nil];
}

@end
