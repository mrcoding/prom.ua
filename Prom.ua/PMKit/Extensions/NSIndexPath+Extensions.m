//
//  NSIndexPath+Extensions.m
//  Calendar
//
//  Created by Yaroslav Babalich on 17.09.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "NSIndexPath+Extensions.h"

@implementation NSIndexPath (Extensions)

- (BOOL)betweenFirstIndexPath:(NSIndexPath *) firstIndexPath
           andSecondIndexPath:(NSIndexPath *) secondIndexPath
{
    if ( ([self compare:firstIndexPath] == NSOrderedDescending) && ([self compare:secondIndexPath] == NSOrderedAscending) ) {
        return YES;
    }
    
    if ( ([self compare:firstIndexPath] == NSOrderedSame) || ([self compare:secondIndexPath] == NSOrderedSame) ) {
        return YES;
    }
    
    return NO;
}

@end
