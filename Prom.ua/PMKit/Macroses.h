//
//  PMKit.h
//  Proma.ua
//
//  Created by Yaroslav Babalich on 27.05.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#ifndef Macroses_h
#define Macroses_h

#define iOSVersion [[[UIDevice currentDevice] systemVersion] floatValue]

#define MGRandom(from, to) arc4random_uniform(to) + from

#define MGRandomColor(a) [UIColor colorWithRed:(float)(((float)LWRandom(0, 255)) / 255.f) \
                                         green:(float)(((float)LWRandom(0, 255)) / 255.f) \
                                          blue:(float)(((float)LWRandom(0, 255)) / 255.f) \
                                         alpha:a]

#define selfWeakCreate __weak typeof(self) selfWeak = self;

#define LSRGBHex(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// Core Data Macros
#define coreDataUIntProperty(NAME, SETTER_NAME) - (NSUInteger)NAME { \
return [[self customValueForKey:NSStringFromSelector(@selector(NAME))] unsignedIntValue]; } \
- (void)SETTER_NAME:(NSUInteger)NAME { \
[self setCustomValue:@(NAME) forKey:NSStringFromSelector(@selector(NAME))];}

#define coreDataIntProperty(NAME, SETTER_NAME) - (NSInteger)NAME { \
return [[self customValueForKey:NSStringFromSelector(@selector(NAME))] integerValue]; } \
- (void)SETTER_NAME:(NSInteger)NAME { \
[self setCustomValue:@(NAME) forKey:NSStringFromSelector(@selector(NAME))];}

#define coreDataDoubleProperty(NAME, SETTER_NAME) - (double)NAME { \
return [[self customValueForKey:NSStringFromSelector(@selector(NAME))] doubleValue]; } \
- (void)SETTER_NAME:(double)NAME { \
[self setCustomValue:@(NAME) forKey:NSStringFromSelector(@selector(NAME))];}

#endif /* Macroses_h */


CG_INLINE
CGRect CGRectScreenRectWithRotation(BOOL consideringRotation) {
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    BOOL needRotate = NO;
    if ([UIDevice isLandscapeInterfaceOrientation]) {
        if (iOSVersion >= 8) {
            needRotate = !consideringRotation; // screen already rotated for iOS >= 8
        } else {
            needRotate = consideringRotation;
        }
    }
    
    if (needRotate) {
        CGFloat tmp = screenFrame.size.width;
        screenFrame.size.width = screenFrame.size.height;
        screenFrame.size.height = tmp;
    }
    
    return screenFrame;
}

CG_INLINE
double floatInBounds(double value, double MIN_VALUE, double MAX_VALUE) {
    if (value < MIN_VALUE) {
        return MIN_VALUE;
    } else if (value > MAX_VALUE) {
        return MAX_VALUE;
    }
    
    return value;
}

CG_INLINE
NSInteger intInBounds(NSInteger value, NSInteger MIN_VALUE, NSInteger MAX_VALUE) {
    if (value < MIN_VALUE) {
        return MIN_VALUE;
    } else if (value > MAX_VALUE) {
        return MAX_VALUE;
    }
    
    return value;
}

CG_INLINE
double distanceBetweenPoints(CGPoint point1, CGPoint point2) {
    double dx = (point1.x - point2.x);
    double dy = (point1.y - point2.y);
    return sqrt(dx*dx + dy*dy);
}
