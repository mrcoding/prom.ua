//
//  MGURLDownloader.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMObservableObject.h"

@class MGURLDownloader;

@protocol MGURLDownloader <NSObject>
@required
- (void)URLDownloaderDidLoad:(MGURLDownloader *)downloader;

@optional
- (void)URLDownloaderDidCancelLoading:(MGURLDownloader *)downloader;

@end

@interface MGURLDownloader : PMObservableObject

+ (instancetype)downloaderWithURL:(NSURL *)url;
+ (instancetype)downloaderWithRequest:(NSURLRequest *)urlRequest;

/**
 * Get last modification date of URL content.
 * Pefrorm this method ONLY in background.
 */
+ (NSDate *)lastModificationDateOfURL:(NSURL *)url;

/**
 * URL for request
 */
@property (nonatomic, readonly)     NSURL           *url;

/**
 * URL connection object which will be used for request.
 */
@property (nonatomic, readonly)     NSURLRequest    *urlRequest;

/**
 * Response data.
 */
@property (nonatomic, readonly)     NSData          *data;

/**
 * Response data in string.
 */
@property (nonatomic, readonly)     NSString        *string;

/**
 * Error after performing last request.
 */
@property (nonatomic, readonly)     NSError         *lastError;

/**
 * Is request finished or in progress.
 */
@property (nonatomic, readonly)     BOOL            finished;

/**
 * Start loading data by specified URL or request.
 */
- (void)startLoading;

/**
 * Cancel loading.
 */
- (void)cancel;

@end
