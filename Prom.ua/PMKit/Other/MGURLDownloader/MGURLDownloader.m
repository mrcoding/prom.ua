//
//  MGURLDownloader.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGURLDownloader.h"

@interface MGURLDownloader () <NSURLConnectionDelegate>

@property (nonatomic, strong)   NSURLRequest            *urlRequest;
@property (nonatomic, strong)   NSError                 *lastError;
@property (nonatomic, assign)   BOOL                    finished;

@property (nonatomic, strong)   NSMutableData           *mutableData;
@property (nonatomic, strong)   NSURLConnection         *connection;

@end

@implementation MGURLDownloader

@dynamic url;
@dynamic data;
@dynamic string;

#pragma mark -
#pragma mark Class Methods

+ (id)downloaderWithURL:(NSURL *)url {
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    return [self downloaderWithRequest:request];
}

+ (id)downloaderWithRequest:(NSURLRequest *)urlRequest {
    MGURLDownloader *result = [[self alloc] init];
    result.urlRequest = urlRequest;
    result.mutableData = [NSMutableData data];
    
    return result;
}

#pragma mark -
#pragma mark Private

/**
 * Get last modification date of ulr content.
 * Pefrorm this method ONLY in background.
 */
+ (NSDate *)lastModificationDateOfURL:(NSURL *)url {
    NSDate *date = nil;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    if ([response respondsToSelector:@selector(allHeaderFields)])
    {
        NSDictionary *dictionary = [response allHeaderFields];
        NSString *lastUpdated = [dictionary valueForKey:@"Last-Modified"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss zzz"];
        date = [formatter dateFromString:lastUpdated];
        /*
         if (([localModificationDate earlierDate:date] == date) && date) {
         NSLog(@"local file is not actual");
         isLatest = NO;
         } else {
         NSLog(@"local file is actual");
         }
         */
        
    } else {
        NSLog(@"Failed to get server response headers");
    }
    
    return date;
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (void)dealloc {
    self.urlRequest = nil;
    self.lastError = nil;
    self.mutableData = nil;
    self.connection = nil;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.finished = YES;
    }
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (NSURL *)url {
    return self.urlRequest.URL;
}

- (void)setConnection:(NSURLConnection *)connection {
    if (connection != _connection) {
        [_connection cancel];
        _connection = connection;
    }
}

- (NSData *)data {
    return [NSData dataWithData:self.mutableData];
}

- (NSString *)string {
    return [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
}

#pragma mark -
#pragma mark Public

- (void)startLoading {
    @synchronized(self) {
        if (!self.finished) {
            return;
        }
        
        self.finished = NO;
    }
    
	self.connection = [NSURLConnection connectionWithRequest:self.urlRequest
                                                    delegate:self];
}

- (void)cancel {
    @synchronized(self) {
        if (self.finished) {
            return;
        }
        
        self.finished = YES;
        self.lastError = nil;
        self.mutableData = nil;
        self.connection = nil; // cancel loading
    }
    
    [self notifyObserversOnMainThreadWithSelector:@selector(URLDownloaderDidCancelLoading:)
                                       withObject:self];
}

#pragma mark -
#pragma mark Private Notifications

- (void)finishedLoading {
    @synchronized(self) {
        self.finished = YES;
    }
    
    [self notifyObserversOnMainThreadWithSelector:@selector(URLDownloaderDidLoad:)
                                       withObject:self];
}

#pragma mark -
#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == self.connection) {
        self.mutableData.length = 0;
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == self.connection) {
		// append the new data
		[self.mutableData appendData:data];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection == self.connection) {
        self.lastError = error;
        [self finishedLoading];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (connection == self.connection) {
        [self finishedLoading];
	}
}

@end
