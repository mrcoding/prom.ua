//
//  MGWeakrefArray.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGWeakReference.h"

// Array that contain the (LSWeakReference *) objects

@interface MGMutableWeakArray : NSMutableArray

@property (nonatomic, readonly) NSArray *weakReferences;

@end
