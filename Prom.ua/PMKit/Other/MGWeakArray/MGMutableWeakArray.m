//
//  MGWeakrefArray.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGMutableWeakArray.h"

@interface MGMutableWeakArray ()
@property (nonatomic, strong)   NSMutableArray      *array;

@end

@implementation MGMutableWeakArray

@dynamic weakReferences;

#pragma mark -
#pragma mark Initializations and Deallocations

- (void)dealloc {
    self.array = nil;
}

- (id)init {
    self = [super init];
    if (self) {
        self.array = [NSMutableArray arrayWithCapacity:1];
    }
    
    return self;
}

- (id)initWithCapacity:(NSUInteger)numItems {
    self = [super init];
    if (self) {
        self.array = [NSMutableArray arrayWithCapacity:numItems];
    }
    
    return self;
}
#pragma mark -
#pragma mark NSArray

- (id)objectAtIndex:(NSUInteger)index {
    MGWeakReference *reference = [self.array objectAtIndex:index];
    
    return reference.object;
}

- (NSUInteger)count {
    return [self.array count];
}

#pragma mark -
#pragma mark NSMutableArray

- (void)removeObject:(id)anObject {
    MGWeakReference *reference = [MGWeakReference referenceWithObject:anObject];
    [self.array removeObject:reference];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [self.array removeObjectAtIndex:index];
}

- (void)removeLastObject {
    [self.array removeLastObject];
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    MGWeakReference *reference = [MGWeakReference referenceWithObject:anObject];
    [self.array insertObject:reference atIndex:index];
}

- (void)addObject:(id)anObject {
    MGWeakReference *reference = [MGWeakReference referenceWithObject:anObject];
    [self.array addObject:reference];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    MGWeakReference *reference = [MGWeakReference referenceWithObject:anObject];
    [self.array replaceObjectAtIndex:index withObject:reference];
}

- (BOOL)containsObject:(id)anObject {
    return [self.array containsObject:anObject];
}

#pragma mark -
#pragma mark Accessors

- (NSArray *)weakReferences {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    for (NSUInteger i = 0; i < [self count]; i++) {
        [result addObject:[self.array objectAtIndex:i]];
    }
    
    return [NSArray arrayWithArray:result];
}

@end