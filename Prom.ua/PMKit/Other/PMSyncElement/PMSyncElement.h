//
//  PMSyncElement.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 9/15/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMSyncElement;

@interface PMSyncElement : NSObject

+ (instancetype)syncElement;

@property (nonatomic, assign) BOOL          finished; // you need to set it to YES after work finished

@property (nonatomic, strong) id            paramObject; // custom user object
@property (nonatomic, assign) BOOL          success; // custom user BOOL
@property (nonatomic, assign) NSInteger     paramInt; // custom user Integer
@property (nonatomic, strong) NSString      *paramString;


/**
 * Wait for |finished| == YES and then set it to NO and perform |block|.
 * You must set |self.finished = YES| in |block| manually.
 */
- (void)queueBlock:(void (^)(PMSyncElement *syncElement, BOOL afterWait))block;

/**
 * Wait for |finished| == YES. Perform this method ONLY in background thread.
 */
- (void)waitUntilFinishedWithTimeout:(NSUInteger)timeout;

@end
