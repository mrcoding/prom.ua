//
//  PMSyncElement.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 9/15/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "PMSyncElement.h"

@implementation PMSyncElement

#pragma mark -
#pragma mark Class Methods

+ (instancetype)syncElement {
    PMSyncElement *result = [[PMSyncElement alloc] init];
    return result;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.finished = YES;
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)queueBlock:(void (^)(PMSyncElement *syncElement, BOOL afterWait))block {
    if (!block) {
        return;
    }
    
    @synchronized(self) {
        if (!self.finished) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                while (YES) {
                    [self waitUntilFinishedWithTimeout:100];
                    @synchronized(self) {
                        if (self.finished) {
                            self.finished = NO;
                            break;
                        }
                    }
                }
                
                block(self, YES);
            });
            return;
        }
        
        self.finished = NO;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        block(self, NO);
    });
}

- (void)waitUntilFinishedWithTimeout:(NSUInteger)timeout {
    CGFloat currentTime = 0;
    CGFloat timeDelta = 0.1; // seconds
    while (!self.finished
           && currentTime < (timeout * 1.0))
    {
        currentTime += timeDelta;
        usleep(timeDelta * 1000000);
    }
}

@end
