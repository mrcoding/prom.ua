//
//  MGObservableObject.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "PMObservableObject.h"
#import "MGMutableWeakArray.h"

#define MGSuppressPerformSelectorLeakWarning(code) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
code; \
_Pragma("clang diagnostic pop") \
} while (0)

@interface PMObservableObject ()

@property (nonatomic, retain)	MGMutableWeakArray	*mutableObservers;

@end


@implementation PMObservableObject

@dynamic observers;
@dynamic target;

#pragma mark -
#pragma mark Initializations and Deallocations

- (void)dealloc {
    self.mutableObservers = nil;
    //[super dealloc];
}

- (id)init {
    self = [super init];
    if (self) {
        self.mutableObservers = [MGMutableWeakArray array];
    }
    
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (NSArray *)observers {
    @synchronized(self) {
        return [self.mutableObservers copy];
    }
}

- (id)target {
    return self;
}

#pragma mark -
#pragma mark Public

- (void)addObserver:(id)observer {
    @synchronized(self) {
        if (![self containObserver:observer]) {
            [self.mutableObservers addObject:observer];
        }
    }
}

- (void)removeObserver:(id)observer {
    @synchronized(self) {
        [self.mutableObservers removeObject:observer];
    }
}

- (void)removeAllObservers {
    @synchronized(self) {
        [self.mutableObservers removeAllObjects];
    }
}

- (BOOL)containObserver:(id)observer {
    return [self.mutableObservers containsObject:observer];
}

#pragma mark -
#pragma mark Private

- (void)notifyObserversWithSelector:(SEL)selector {
    MGSuppressPerformSelectorLeakWarning(
    for (id <NSObject> observer in self.observers) {
        if ([observer respondsToSelector:selector]) {
            [observer performSelector:selector withObject:self.target];
        }
    });
}

- (void)notifyObserversWithSelector:(SEL)selector withObject:(id)object {
    MGSuppressPerformSelectorLeakWarning(
    for (id<NSObject> observer in self.observers) {
        if ([observer respondsToSelector:selector]) {
            [observer performSelector:selector withObject:object];
        }
    });
}

- (void)notifyObserversWithSelector:(SEL)selector
                         withObject:(id)object1
                         withObject:(id)object2
{
    MGSuppressPerformSelectorLeakWarning(
    for (id<NSObject> observer in self.observers) {
        if ([observer respondsToSelector:selector]) {
            [observer performSelector:selector withObject:object1 withObject:object2];
        }
    });
}

- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyObserversWithSelector:selector];
    });
}

- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector withObject:(id)object {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyObserversWithSelector:selector withObject:object];
    });
}

- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector
                                     withObject:(id)object1
                                     withObject:(id)object2
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self notifyObserversWithSelector:selector withObject:object1 withObject:object2];
    });
}

@end
