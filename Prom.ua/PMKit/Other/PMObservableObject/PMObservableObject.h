//
//  MGObservableObject.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMObservableObject : NSObject

/**
 * Array with observers.
 */
@property (nonatomic, readonly)	NSArray			*observers;

/**
 * Target is the object, that would be notified.
 * Returns self by default.
 */
@property (nonatomic, readonly) id <NSObject>   target;

/**
 * Observable object maintains weak links to its observers
 * you are responsible to remove yourself as an observer,
 * when you no longer need to observe the object
 */
- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;
- (void)removeAllObservers;
- (BOOL)containObserver:(id)observer;

/**
 * These methods should only be called in child classes.
 * Call these methods to notify the observers by calling
 * their selectors.
 */
- (void)notifyObserversWithSelector:(SEL)selector;
- (void)notifyObserversWithSelector:(SEL)selector withObject:(id)object;
- (void)notifyObserversWithSelector:(SEL)selector
                         withObject:(id)object1
                         withObject:(id)object2;

- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector;
- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector withObject:(id)object;
- (void)notifyObserversOnMainThreadWithSelector:(SEL)selector
                                     withObject:(id)object1
                                     withObject:(id)object2;
@end
