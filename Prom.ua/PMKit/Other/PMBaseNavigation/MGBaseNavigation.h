//
//  MGBaseNavigation.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 03.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGBaseNavigation : NSObject

+ (instancetype)navigation;

@property (nonatomic, strong) UIWindow                  *window;
@property (nonatomic, strong) UINavigationController    *navController;

/**
 * Go to view controller
 */
- (void)showController:(UIViewController *) controller;
- (void)showController:(UIViewController *)controller animation:(BOOL) animation;
- (void)presentController:(UIViewController *) controller;

/**
 * Go to prev view controller
 */
- (void)goBack;

@end
