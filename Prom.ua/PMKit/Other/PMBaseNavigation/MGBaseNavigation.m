//
//  MGBaseNavigation.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 03.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "MGBaseNavigation.h"

@interface MGBaseNavigation()

@property (nonatomic, strong) UIViewController *currentViewController;
@property (nonatomic, strong) UIViewController *rootViewController;

@end

@implementation MGBaseNavigation

#pragma mark -
#pragma mark - Class methods
+ (instancetype)navigation{
    static MGBaseNavigation *nav = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nav = [[MGBaseNavigation alloc] init];
    });
    return nav;
}

#pragma mark -
#pragma mark - Accessors
- (UIViewController *)currentViewController{
   return self.navController.visibleViewController;
}

- (void)setNavController:(UINavigationController *) navController{
    _navController = navController;
    [navController setNavigationBarHidden:YES];
    self.rootViewController = navController.topViewController;
}

#pragma mark -
#pragma mark - Public methods
- (void)showController:(UIViewController *) controller{
    [self showController:controller animation:YES];
}

- (void)showController:(UIViewController *)controller animation:(BOOL) animation{
    [self.navController pushViewController:controller animated:animation];
    
}

- (void)presentController:(UIViewController *) controller{
    [self.navController presentViewController:controller
                                     animated:YES
                                   completion:^{
                                       
                                   }];
}

- (void)goBack{
    [self.navController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - Private methods

@end
