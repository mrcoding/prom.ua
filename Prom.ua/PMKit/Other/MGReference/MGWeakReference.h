//
//  MGReference.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MGWeakReference : NSObject <NSCopying>

@property (nonatomic, assign, readonly) id object;

+ (instancetype)referenceWithObject:(NSObject *)theObject;

@end
