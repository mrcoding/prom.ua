//
//  MGReference.h
//  PxToday
//
//  Created by Yaroslav Babalich on 4/22/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGWeakReference.h"

@interface MGWeakReference ()
@property (nonatomic, assign, readwrite) id object;
@end

@implementation MGWeakReference

#pragma mark -
#pragma mark Class Methods

+ (id)referenceWithObject:(id)theObject {
    MGWeakReference *reference = [[self alloc] init];
    reference.object = theObject;
    
    return reference;
}

#pragma mark -
#pragma mark Public

- (NSString *)description {
    return [NSString stringWithFormat:@"%@: %@", [super description], self.object];
}

#pragma mark -
#pragma mark Comparison

- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[self class]]) {
        MGWeakReference *reference = object;
        
        return self.object == reference.object;
    }
    
    return NO;
}

- (NSUInteger)hash {
    return (NSUInteger)self.object;
}

#pragma mark -
#pragma mark NSCopying

- (id)copyWithZone:(NSZone *)zone {
    MGWeakReference *reference = [[self class] referenceWithObject:self.object]; //retain];
    
    return reference;
}

@end
