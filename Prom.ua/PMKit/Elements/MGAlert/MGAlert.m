//
//  PMAlert.m
//  PxToday
//
//  Created by Yaroslav Babalich on 6/12/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGAlert.h"

@interface MGAlert () <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) UIAlertView               *alertView;
@property (nonatomic, assign) NSUInteger                selectedButtonIndex;
@property (nonatomic, copy) MGAlertCompletionBlock      completionBlock;
@property (nonatomic, assign) BOOL                      finished;
@property (nonatomic, assign) BOOL                      noCancelButton;

@end

@implementation MGAlert


@dynamic cancelPressed;
@dynamic secondPressed;

#pragma mark -
#pragma mark Class methods

+ (instancetype)showAlertWithTitle:(NSString *)title
                           message:(NSString *)message
                       cancelTitle:(NSString *)cancelTitle
                       secondTitle:(NSString *)secondTitle
                        completion:(MGAlertCompletionBlock)completion
{
    __block MGAlert *alert = [self alertWithTitle:title
                                          message:message
                                      cancelTitle:cancelTitle
                                      secondTitle:secondTitle
                                       completion:^(MGAlert *selfObject) {
                                           if (completion) {
                                               completion(alert);
                                           }
                                           
                                           alert = nil; // dealloc
                                       }];
    [alert showAlert];
    
    return alert;
}

+ (instancetype)alertWithTitle:(NSString *)title
                       message:(NSString *)message
                   cancelTitle:(NSString *)cancelTitle
                   secondTitle:(NSString *)secondTitle
                    completion:(MGAlertCompletionBlock)completion
{
    MGAlert *alert = [[self alloc] init];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:alert
                                              cancelButtonTitle:cancelTitle
                                              otherButtonTitles:secondTitle, nil];
    alert.noCancelButton = !cancelTitle;
    alert.alertView = alertView;
    alert.finished = NO;
    [alert setupCompletionBlock:completion];
    
    return alert;
}

+ (instancetype)showAlertWithMessage:(NSString *)message {
    return [self showAlertWithTitle:nil message:message cancelTitle:nil secondTitle:@"Ok"
                         completion:nil];
}

+ (instancetype)showTextFieldAlertWithMessage:(NSString *) message completion:(MGAlertCompletionBlock)completion {
    __block MGAlert *alert = [self alertWithTitle:nil
                                          message:message
                                      cancelTitle:NSLocalizedString(@"Cancel", nil)
                                      secondTitle:NSLocalizedString(@"Ok", nil)
                                       completion:^(MGAlert *alertView) {
                                           if (completion) {
                                               completion(alertView);
                                           }
                                           
                                           alert = nil;
                                       }];
    alert.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField = [alert.alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeEmailAddress;
    [alert showAlert];
    return alert;
}

+ (instancetype)showAlertWithMessage:(NSString *)message completion:(MGAlertCompletionBlock)completion {
    return [self showAlertWithTitle:nil message:message cancelTitle:nil secondTitle:@"Ok"
                         completion:completion];
}

#pragma mark -
#pragma mark Accessors

- (BOOL)cancelPressed {
    return (!self.noCancelButton && self.selectedButtonIndex == 0);
}

- (BOOL)secondPressed {
    return (!self.noCancelButton && self.selectedButtonIndex == 1)
    || (self.noCancelButton && self.selectedButtonIndex == 0);
}

#pragma mark -
#pragma mark Public

- (void)showAlert {
    @synchronized(self) {
        if (!self.finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                [self.alertView show];
            });
        }
    }
}

- (void)closeAlertWithButtonIndex:(NSUInteger)index animated:(BOOL)animated {
    [self.alertView dismissWithClickedButtonIndex:index animated:animated];
}

#pragma mark -
#pragma mark Private

- (void)setupCompletionBlock:(MGAlertCompletionBlock)completion {
    self.completionBlock = completion;
}

#pragma mark -
#pragma mark Private UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    @synchronized(self) {
        if (self.alertView == alertView) {
            self.selectedButtonIndex = buttonIndex;
            self.finished = YES;
            self.completionBlock(self);
        }
    }
}

@end
