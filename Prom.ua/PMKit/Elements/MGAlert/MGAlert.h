//
//  MGAlert.h
//  Learn Words
//
//  Created by Yaroslav Babalich on 23.01.16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MGAlert;

typedef void (^MGAlertCompletionBlock)(MGAlert *alert);

@interface MGAlert : NSObject


/**
 * Show message with parameters. Not needed to retain result object.
 */
+ (instancetype)showAlertWithTitle:(NSString *)title
                           message:(NSString *)message
                       cancelTitle:(NSString *)cancelTitle
                       secondTitle:(NSString *)secondTitle
                        completion:(MGAlertCompletionBlock)completion;

/**
 * Create message object, you need to show it manualy.
 */
+ (instancetype)alertWithTitle:(NSString *)title
                       message:(NSString *)message
                   cancelTitle:(NSString *)cancelTitle
                   secondTitle:(NSString *)secondTitle
                    completion:(MGAlertCompletionBlock)completion;


+ (instancetype)showAlertWithMessage:(NSString *)message;
+ (instancetype)showTextFieldAlertWithMessage:(NSString *) message completion:(MGAlertCompletionBlock) completion;
+ (instancetype)showAlertWithMessage:(NSString *)message completion:(MGAlertCompletionBlock)completion;

@property (nonatomic, readonly) UIAlertView     *alertView;
@property (nonatomic, readonly) NSUInteger      selectedButtonIndex;
@property (nonatomic, readonly) BOOL            cancelPressed;
@property (nonatomic, readonly) BOOL            secondPressed;

@property (nonatomic, readonly) BOOL            finished;

- (void)showAlert;
- (void)closeAlertWithButtonIndex:(NSUInteger)index animated:(BOOL)animated;

@end
