//
//  MGImageModel.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMObservableObject.h"

@class MGImageModel;

@protocol MGImageModelProtocol <NSObject>
@required
- (void)imageModelDidLoad:(MGImageModel *)imageModel;

@optional
- (void)imageModelDidCancelLoading:(MGImageModel *)imageModel;

@end

@interface MGImageModel : PMObservableObject

/**
 * URL of image.
 */
@property (nonatomic, readonly)     NSURL       *URL;

/**
 * Loaded image.
 */
@property (nonatomic, readonly)     UIImage     *image;

/**
 * Is image loading is finished.
 */
@property (nonatomic, readonly)     BOOL        finished;

/**
 * get image URL from specified image name
 */
+ (NSURL *)imageURLFromName:(NSString *)name;

/**
 * Load image from main bundle.
 * |name| is consists of subfolders (path), name and extension of image
 */
+ (id)imageWithName:(NSString *)name;

/**
 * load image from path
 */
+ (id)imageWithPath:(NSString *)path;

/**
 * Load image from URL
 */
+ (id)imageWithURL:(NSURL *)url;

/**
 * init object with URL
 */
- (id)initWithURL:(NSURL *)url;

/**
 * init object with URL and file name for saving to disk
 */
- (id)initWithURL:(NSURL *)url fileName:(NSString *)fileName;


- (void)addObserver:(NSObject<MGImageModelProtocol> *)observer;

/**
 * Asynchronous load image.
 * Notification with selectors in protocol |IDPModelObserver|
 */
- (void)load;

/**
 * Wait until loading of image is finished with specified timeout in seconds.
 * Perform this method ONLY in background.
 */
- (void)waitUntilLoadFinishedWithTimeout:(NSUInteger)timeout;

/**
 * Cancel loading of image (if loading state)
 * If current state is |IDPModelLoading| then
 * observers will be notified, otherwise - no
 */
- (void)cancel;

/**
 * Remove chached image of current image URL.
 */
- (void)removeCacheForImage;

@end
