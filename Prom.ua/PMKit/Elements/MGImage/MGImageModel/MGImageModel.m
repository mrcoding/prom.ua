//
//  MGImageModel.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGImageModel.h"
#import "MGImageCache.h"
#import "MGURLDownloader.h"

static NSString * const kLSSerializationPropertyURL = @"URL";
static NSString * const kLSSerializationPropertyFileName = @"fileName";


@interface MGImageModel () <MGURLDownloader>
@property (nonatomic, strong)       NSURL               *URL;
@property (nonatomic, strong)       NSString            *fileName;
@property (nonatomic, strong)       UIImage             *image;
@property (nonatomic, assign)       BOOL                finished;

@property (nonatomic, strong)       MGURLDownloader     *downloader;

@end

@implementation MGImageModel

+ (NSURL *)imageURLFromName:(NSString *)name {
    NSString *fileName = [name stringByDeletingPathExtension];
    NSString *extension = [name pathExtension];
    /*
     NSString *fileName = [[name lastPathComponent] stringByDeletingPathExtension];
     NSString *subdirectory = [name stringByDeletingLastPathComponent];
     NSURL *url = [[NSBundle mainBundle] URLForResource:fileName
     withExtension:extension
     subdirectory:subdirectory];
     */
    NSURL *url = [[NSBundle mainBundle] URLForResource:fileName withExtension:extension];
    
    return url;
}

+ (id)imageWithName:(NSString *)name {
    NSURL *url = [self imageURLFromName:name];
    
    return [self imageWithURL:url];
}

+ (id)imageWithPath:(NSString *)path {
    return [self imageWithURL:[NSURL URLWithString:path]];
}

+ (id)imageWithURL:(NSURL *)url {
    return [[self alloc] initWithURL:url];
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (void)dealloc {
    [[MGImageCache mainCache] removeImageWithURL:self.URL];
    self.image = nil;
    self.URL = nil;
    self.fileName = nil;
    self.downloader = nil;
}

- (id)init {
    self = [super init];
    if (self) {
        self.fileName = nil;
        self.finished = YES;
    }
    
    return self;
}

- (id)initWithURL:(NSURL *)url {
    self = [self initWithURL:url fileName:nil];
    
    return self;
}

- (id)initWithURL:(NSURL *)url
      fileName:(NSString *)fileName
{
    MGImageCache *cache = [MGImageCache mainCache];
    MGImageModel *result = [cache imageWithURL:url];

    if (result) {
        return result;
    }
    
    self = [self init];
    self.URL = url;
    if (!fileName
        || [fileName length] <= 0) {
        fileName = [self nameOfCacheFile];
    }
    
    self.fileName = fileName;
    [cache addImage:self
            withURL:url];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.URL forKey:kLSSerializationPropertyURL];
    [aCoder encodeObject:self.fileName forKey:kLSSerializationPropertyFileName];
    [self saveImageToDisk:self.image];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSURL *url = [aDecoder decodeObjectForKey:kLSSerializationPropertyURL];
    NSString *fileName = [aDecoder decodeObjectForKey:kLSSerializationPropertyFileName];
    self = [self initWithURL:url fileName:fileName];
    
    return self;
}


#pragma mark -
#pragma mark Accessors

- (void)setDownloader:(MGURLDownloader *)urlConnection {
    if (urlConnection == _downloader) {
        return;
    }
    
    [_downloader cancel];
    [_downloader removeObserver:self];
    _downloader = urlConnection;
    [_downloader addObserver:self];
}


#pragma mark -
#pragma mark Public

- (void)load {
    @synchronized(self) {
        if (!self.finished) {
            return;
        }
        
        self.finished = NO;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self backgroundImageLoad];
    });
}

- (void)waitUntilLoadFinishedWithTimeout:(NSUInteger)timeout {
    CGFloat currentTime = 0;
    CGFloat timeDelta = 0.1; // seconds
    while (!self.finished
           && currentTime < (timeout * 1.0))
    {
        currentTime += timeDelta;
        usleep(timeDelta * 1000000);
    }
}

- (void)cancel {
    [self.downloader cancel];
    [self cancelledLoading];
}

- (void)removeCacheForImage {
    [[MGImageCache mainCache] removeCacheFileWithName:[self nameOfCacheFile]];
}

- (void)addObserver:(NSObject<MGImageModelProtocol> *)observer {
    [super addObserver:observer];
}

#pragma mark -
#pragma mark Private

/**
 * Load from cache or internet/local file.
 */
- (void)backgroundImageLoad {
    // loading image...
    self.image = nil;
    
    if (!self.URL) {
        NSLog(@"*** ERROR! Image URL is empty");
        [self finishedLoading];
    }
    
    UIImage *image = [self loadFromDisk]; // try to load from cache or disk
    self.image = image;
    
    if (!image) {   // if not cached then try to load from internet
        dispatch_async(dispatch_get_main_queue(), ^{
            self.downloader = [MGURLDownloader downloaderWithURL:self.URL];
            [self.downloader startLoading];
        });
    } else {    // loaded from cache
        [self finishedLoading];
    }
}

- (void)saveImageToDisk:(UIImage *)image {
    [[MGImageCache mainCache] saveImage:image
                               fileName:[self nameOfCacheFile]];
}

- (UIImage *)loadFromDisk {
    if ([self isLocalImage]) {
        return [UIImage imageWithContentsOfFile:self.URL.path];
    }
    
    return [[MGImageCache mainCache] loadImageFromCacheFileWithName:[self nameOfCacheFile]];
}

- (BOOL)isLocalImage {
    return [[NSFileManager defaultManager] fileExistsAtPath:self.URL.path];
}

// get file name of image cache from |self.fileName| or from |self.URL|
- (NSString *)nameOfCacheFile {
    NSString *fileName = self.fileName;
    if (fileName
        && [fileName length] > 0) {
        return fileName;
    }
    
    MGImageCache *cache = [MGImageCache mainCache];
    NSString *result = [cache hashFileNameOfString:[self.URL absoluteString]];
    
    return result;
}

#pragma mark -
#pragma mark Private Notifications

- (void)finishedLoading {
    @synchronized(self) {
        self.finished = YES;
    }
    
    [self notifyObserversOnMainThreadWithSelector:@selector(imageModelDidLoad:)
                                       withObject:self];
}

- (void)cancelledLoading {
    @synchronized(self) {
        if (self.finished) {
            return;
        }
        
        self.finished = YES;
    }
    
    [self notifyObserversOnMainThreadWithSelector:@selector(imageModelDidCancelLoading:)
                                       withObject:self];
}

#pragma mark -
#pragma mark LSURLConnection Model methods

- (void)URLDownloaderDidLoad:(MGURLDownloader *)downloader {
    if (self.downloader == downloader) {
        //NSLog(@"--- modelDidLoad %@", self.class);
        NSData *imageData = self.downloader.data;
        UIImage *image = [UIImage imageWithData:imageData];
        
        if (image) {
            self.image = image;
            [self saveImageToDisk:image]; // make cache of loaded image
        }
        
        [self finishedLoading];
    }
}

- (void)URLDownloaderDidCancelLoading:(MGURLDownloader *)downloader {
    if (self.downloader == downloader) {
        [self cancelledLoading];
    }
}

@end
