//
//  MGImageCache.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MGImageCache : NSObject
@property (nonatomic, copy, readonly)   NSString              *cacheDirectoryPath;

// get main images cache instance (singleton)
+ (id)mainCache;

- (id)imageWithURL:(NSURL *)url;
- (void)addImage:(id)image withURL:(NSURL *)url;
- (void)removeImageWithURL:(NSURL *)url;
- (void)removeAll;

// cache file operations

- (NSURL *)saveImage:(UIImage *)image fileName:(NSString *)fileName;
- (UIImage *)loadImageFromCacheFileWithName:(NSString *)fileName;
- (NSString *)hashFileNameOfString:(NSString *)string;

- (NSDateComponents *)lastModificationDifferenceTimeOfCacheFile:(NSString *)fileName
                                                  calendarUnits:(NSCalendarUnit)units;
- (void)removeCacheFileWithName:(NSString *)fileName;
- (void)removeCacheFiles;

@end
