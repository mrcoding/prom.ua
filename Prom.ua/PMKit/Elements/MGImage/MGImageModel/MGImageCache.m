//
//  MGImageCache.h
//  PxToday
//
//  Created by Yaroslav Babalich on 5/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGImageCache.h"
#import "MGMutableWeakArray.h"
#import "MGWeakReference.h"
#import "Extensions.h"

static NSString * const kMUImageStoreDirectory = @"imagesCache";
static NSString * const kMUImageStoreName = @"image_%@.png";

static MGImageCache *__sharedObject = nil;


@interface MGImageCache ()
@property (nonatomic, retain) NSMutableDictionary   *mutableCache;
@property (nonatomic, copy)   NSString              *cacheDirectoryPath;

@end


@implementation MGImageCache

#pragma mark -
#pragma mark Singleton Methods

+ (id)mainCache {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedObject = [[self alloc] init];
    });
    
    return __sharedObject;
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (id)init {
    self = [super init];
    if (self) {
        self.mutableCache = [NSMutableDictionary dictionary];
        [self setupCacheDirectoryPath];
    }
    
    return self;
}

#pragma mark -
#pragma mark Public

- (id)imageWithURL:(NSURL *)url {
    //NSLog(@"--- %@: get image", self.class);
    MGWeakReference *result = nil;
    @synchronized(self.mutableCache) {
        result = url ? [self.mutableCache objectForKey:url] : nil;
    }
    
    return result.object;
}

- (void)addImage:(id)image withURL:(NSURL *)url {
    if (url) {
        if (![self imageWithURL:url]) {
            @synchronized(self.mutableCache) {
                //NSLog(@"--- %@: add", self.class);
                MGWeakReference *imageReference = [MGWeakReference referenceWithObject:image];
                [self.mutableCache setObject:imageReference
                                      forKey:url];
            }
        }
    }
}

- (void)removeImageWithURL:(NSURL *)url {
    if (url) {
        @synchronized(self.mutableCache) {
            //NSLog(@"--- %@: remove", self.class);
            [self.mutableCache removeObjectForKey:url];
        }
    }
}

- (void)removeAll {
    @synchronized(self.mutableCache) {
        [self.mutableCache removeAllObjects];
    }
}


- (NSURL *)saveImage:(UIImage *)image
            fileName:(NSString *)fileName
{
    if (!image || !fileName) {
        return nil;
    }
    
    NSString *path = self.cacheDirectoryPath;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:path
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:nil];
    path = [path stringByAppendingPathComponent:fileName];
    //NSLog(@"--- %@", path);
    
    NSData *imageData = UIImagePNGRepresentation(image);
    if (imageData && ![imageData writeToFile:path atomically:YES]) {
        NSLog(@"*** ERROR! Save \"%@\" failed", path);
    }
    
    return [NSURL fileURLWithPath:path];
}

- (UIImage *)loadImageFromCacheFileWithName:(NSString *)fileName {
    NSString *path = self.cacheDirectoryPath;
    path = [path stringByAppendingPathComponent:fileName];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

- (NSDateComponents *)lastModificationDifferenceTimeOfCacheFile:(NSString *)fileName
                                                  calendarUnits:(NSCalendarUnit)units
{
    NSString *path = self.cacheDirectoryPath;
    path = [path stringByAppendingPathComponent:fileName];
    NSDateComponents *result = nil;
    
    NSDate *fileDate = nil;
    NSError *error = nil;
    [[NSURL URLWithString:path] getResourceValue:&fileDate
                                          forKey:NSURLContentModificationDateKey
                                           error:&error];
    if (!error) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        result = [calendar components:units
                             fromDate:fileDate
                               toDate:[NSDate date]
                              options:0];
    }
    
    return result;
}

// get file name of image cache from |self.fileName| or from |self.URL|
- (NSString *)hashFileNameOfString:(NSString *)string {
    NSString *result = [NSString stringWithFormat:(id)kMUImageStoreName,
                        [string MD5String]];
    
    return result;
}

- (void)removeCacheFileWithName:(NSString *)fileName {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *path = self.cacheDirectoryPath;
    [fileMgr removeItemAtPath:[path stringByAppendingPathComponent:fileName] error:NULL];
}

- (void)removeCacheFiles {
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *path = self.cacheDirectoryPath;
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:path error:nil];
    for (NSString *filename in fileArray)  {
        [fileMgr removeItemAtPath:[path stringByAppendingPathComponent:filename] error:NULL];
    }
}

#pragma mark -
#pragma mark Private

- (void)setupCacheDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths firstObject];
    path = [path stringByAppendingPathComponent:kMUImageStoreDirectory];
    self.cacheDirectoryPath = path;
}


@end
