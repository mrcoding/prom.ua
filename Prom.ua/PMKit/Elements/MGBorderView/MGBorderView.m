//
//  MGBorderView.h
//  PxToday
//
//  Created by Yaroslav Babalich on 6/9/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "MGBorderView.h"

@interface MGBorderView ()
@property (nonatomic, assign) CGSize    oldFrameSize;
@property (nonatomic, strong) UIColor   *backgroundColor;

@end

@implementation MGBorderView

#pragma mark -
#pragma mark Initializations and Deallocations

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self  setup];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)updateConstraints {
    [super updateConstraints];
    [self setNeedsDisplay];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (!CGSizeEqualToSize(self.frame.size, self.oldFrameSize)) {
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath; // for speed up
        self.oldFrameSize = self.frame.size;
        [self setNeedsDisplay];
    }
}

#pragma mark -
#pragma mark Accessors

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    // no super method call
    if (backgroundColor != _backgroundColor) {
        _backgroundColor = [backgroundColor copy];
        [self setNeedsDisplay];
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    if (_backgroundImage != backgroundImage) {
        _backgroundImage = backgroundImage;
        [self setNeedsDisplay];
    }
}

- (void)setShadowRadius:(CGFloat)shadowRadius {
    if (_shadowRadius != shadowRadius) {
        _shadowRadius = shadowRadius;
        [self updateShadow];
    }
}

- (void)setShadowOpacity:(CGFloat)shadowOpacity {
    if (_shadowOpacity != shadowOpacity) {
        _shadowOpacity = shadowOpacity;
        [self updateShadow];
    }
}

- (void)setShadowOffsetX:(CGFloat)shadowOffsetX {
    if (_shadowOffsetX != shadowOffsetX) {
        _shadowOffsetX = shadowOffsetX;
        [self updateShadow];
    }
}

- (void)setShadowOffsetY:(CGFloat)shadowOffsetY {
    if (_shadowOffsetY != shadowOffsetY) {
        _shadowOffsetY = shadowOffsetY;
        [self updateShadow];
    }
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    self.leftBorderColor = borderColor;
    self.topBorderColor = borderColor;
    self.rightBorderColor = borderColor;
    self.bottomBorderColor = borderColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    self.leftBorderWidth = borderWidth;
    self.topBorderWidth = borderWidth;
    self.rightBorderWidth = borderWidth;
    self.bottomBorderWidth = borderWidth;
}

- (void)setLeftBorderColor:(UIColor *)leftBorderColor {
    _leftBorderColor = leftBorderColor;
    [self setNeedsDisplay];
}

- (void)setTopBorderColor:(UIColor *)topBorderColor {
    _topBorderColor = topBorderColor;
    [self setNeedsDisplay];
}

- (void)setRightBorderColor:(UIColor *)rightBorderColor {
    _rightBorderColor = rightBorderColor;
    [self setNeedsDisplay];
}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor {
    _bottomBorderColor = bottomBorderColor;
    [self setNeedsDisplay];
}

- (void)setLeftBorderWidth:(CGFloat)leftBorderWidth {
    _leftBorderWidth = leftBorderWidth;
    [self setNeedsDisplay];
}

- (void)setTopBorderWidth:(CGFloat)topBorderWidth {
    _topBorderWidth = topBorderWidth;
    [self setNeedsDisplay];
}

- (void)setRightBorderWidth:(CGFloat)rightBorderWidth {
    _rightBorderWidth = rightBorderWidth;
    [self setNeedsDisplay];
}

- (void)setBottomBorderWidth:(CGFloat)bottomBorderWidth {
    _bottomBorderWidth = bottomBorderWidth;
    [self setNeedsDisplay];
}

#pragma mark -
#pragma mark Private

- (void)setup {
    self.borderColor = [UIColor grayColor];
    self.borderWidth = 0;
    
    self.shadowRadius = 0;
    self.shadowOpacity = 0;
    self.shadowOffsetX = 0;
    self.shadowOffsetY = 0;
    
    UIColor *backgroundColor = self.backgroundColor;
    [super setBackgroundColor:[UIColor clearColor]]; // super method, for transparency support
    self.backgroundColor = backgroundColor;
}

- (void)updateShadow {
    self.clipsToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(self.shadowOffsetX, self.shadowOffsetY);
    self.layer.shadowRadius = self.shadowRadius;
    self.layer.shadowOpacity = self.shadowOpacity;
}

/**
 * Drawing graphics
 */
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineJoin(context, kCGLineJoinMiter);
    CGContextSetShouldAntialias(context, YES);
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);
    CGContextFillRect(context, rect);
    if (self.backgroundImage) { // if need to draw background image
        [self.backgroundImage drawInRect:rect];
    }
    
    CGFloat minx = CGRectGetMinX(rect);
    CGFloat maxx = CGRectGetMaxX(rect);
    CGFloat miny = CGRectGetMinY(rect);
    CGFloat maxy = CGRectGetMaxY(rect);
    
    CGContextMoveToPoint(context, minx, miny);
    CGContextSetStrokeColorWithColor(context, self.topBorderColor.CGColor);
    CGContextSetLineWidth(context, self.topBorderWidth);
    CGContextAddLineToPoint(context, maxx, miny);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, maxx, miny);
    CGContextSetStrokeColorWithColor(context, self.rightBorderColor.CGColor);
    CGContextSetLineWidth(context, self.rightBorderWidth);
    CGContextAddLineToPoint(context, maxx, maxy);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, maxx, maxy);
    CGContextSetStrokeColorWithColor(context, self.bottomBorderColor.CGColor);
    CGContextSetLineWidth(context, self.bottomBorderWidth);
    CGContextAddLineToPoint(context, minx, maxy);
    CGContextStrokePath(context);
    
    CGContextMoveToPoint(context, minx, maxy);
    CGContextSetStrokeColorWithColor(context, self.leftBorderColor.CGColor);
    CGContextSetLineWidth(context, self.leftBorderWidth);
    CGContextAddLineToPoint(context, minx, miny);
    CGContextStrokePath(context);
}



@end
