//
//  MGBorderView.h
//  PxToday
//
//  Created by Yaroslav Babalich on 6/9/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MGBorderView : UIView
//@property (nonatomic, assign) IBInspectable BOOL      withRightArrow;

/**
 * Background image
 */
@property (nonatomic, strong) IBInspectable UIImage     *backgroundImage;

// shadow
@property (nonatomic, assign) IBInspectable CGFloat     shadowRadius;
@property (nonatomic, assign) IBInspectable CGFloat     shadowOpacity;
@property (nonatomic, assign) IBInspectable CGFloat     shadowOffsetX;
@property (nonatomic, assign) IBInspectable CGFloat     shadowOffsetY;

// colors
@property (nonatomic, strong) IBInspectable UIColor     *borderColor;
@property (nonatomic, strong) IBInspectable UIColor     *topBorderColor;
@property (nonatomic, strong) IBInspectable UIColor     *bottomBorderColor;
@property (nonatomic, strong) IBInspectable UIColor     *leftBorderColor;
@property (nonatomic, strong) IBInspectable UIColor     *rightBorderColor;

// widths
@property (nonatomic, assign) IBInspectable CGFloat     borderWidth;
@property (nonatomic, assign) IBInspectable CGFloat     topBorderWidth;
@property (nonatomic, assign) IBInspectable CGFloat     bottomBorderWidth;
@property (nonatomic, assign) IBInspectable CGFloat     leftBorderWidth;
@property (nonatomic, assign) IBInspectable CGFloat     rightBorderWidth;

@end
