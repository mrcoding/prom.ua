//
//  PMTextField.h
//  PxToday
//
//  Created by Yaroslav Babalich on 11/27/15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMTextFieldDelegate.h"

@interface PMTextField : UITextField

@property (nonatomic, strong) PMTextFieldDelegate *delegateObject;

- (void)animateWrongData;

@end
