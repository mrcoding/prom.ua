//
//  PMTextFieldDelegate.h
//  PxToday
//
//  Created by Yaroslav Babalich on 11/27/15.
//  Copyright © 2015 PxToday. All rights reserved.
//


#import "PMTextFieldDelegate.h"

@interface PMTextFieldDelegate ()
@property (nonatomic, copy) PMTextFieldOnEditing        onEditingBlock;
@property (nonatomic, copy) PMTextFieldOnAction         onStartEditingBlock;
@property (nonatomic, copy) PMTextFieldOnAction         onEndEditingBlock;
@property (nonatomic, copy) PMTextFieldOnActionBOOL     onEnterButtonBlock;
@property (nonatomic, copy) PMTextFieldOnActionBOOL     onClearBlock;

@end

@implementation PMTextFieldDelegate

#pragma mark -
#pragma mark Class Methods

+ (instancetype)delegateForTextField:(UITextField *)textField {
    PMTextFieldDelegate *result = [[PMTextFieldDelegate alloc] init];
    result.textField = textField;
    textField.delegate = result;
    
    return result;
}

#pragma mark -
#pragma mark Public

- (void)onEditing:(PMTextFieldOnEditing)block {
    self.onEditingBlock = block;
}

- (void)onStartEditing:(PMTextFieldOnAction)block {
    self.onStartEditingBlock = block;
}

- (void)onEndEditing:(PMTextFieldOnAction)block {
    self.onEndEditingBlock = block;
}

- (void)onEnterButton:(PMTextFieldOnActionBOOL)block {
    self.onEnterButtonBlock = block;
}

- (void)onClear:(PMTextFieldOnActionBOOL)block {
    self.onClearBlock = block;
}

#pragma mark -
#pragma mark Delegate UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)replacementString
{
    self.textField = textField;
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:replacementString];
    
    if (self.onEditingBlock) {
        return self.onEditingBlock(self, resultString, range, replacementString);
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.textField = textField;
    if (self.onStartEditingBlock) {
        self.onStartEditingBlock(self);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.textField = textField;
    if (self.onEndEditingBlock) {
        self.onEndEditingBlock(self);
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    self.textField = textField;
    if (self.onEnterButtonBlock) {
        return self.onEnterButtonBlock(self);
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    self.textField = textField;
    if (self.onClearBlock) {
        return self.onClearBlock(self);
    }
    
    return YES;
}

@end
