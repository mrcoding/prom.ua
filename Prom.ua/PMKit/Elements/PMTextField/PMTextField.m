//
//  PMTextField.h
//  PxToday
//
//  Created by Yaroslav Babalich on 11/27/15.
//  Copyright © 2015 PxToday. All rights reserved.

#import "PMTextField.h"

@implementation PMTextField

#pragma mark -
#pragma mark Initializations and Deallocations

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupLabel];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupLabel];
    }
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (void)setDelegateObject:(PMTextFieldDelegate *)delegateObject {
    _delegateObject.textField = nil;
    _delegateObject = delegateObject;
    self.delegate = delegateObject;
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate {
    [super setDelegate:delegate];
}

#pragma mark -
#pragma mark Public

- (void)animateWrongData {
    self.backgroundColor = [UIColor redColor];
    [UIView animateWithDuration:2.0 animations:^{
        self.backgroundColor = [UIColor clearColor];
    }];
}

#pragma mark -
#pragma mark Private

- (void)setupLabel {
    self.delegateObject = [PMTextFieldDelegate delegateForTextField:self];
}

@end
