//
//  PMTextFieldDelegate.h
//  PxToday
//
//  Created by Yaroslav Babalich on 11/27/15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTextFieldDelegate;

typedef void (^PMTextFieldOnAction)(PMTextFieldDelegate *delegate);
typedef BOOL (^PMTextFieldOnActionBOOL)(PMTextFieldDelegate *delegate);
typedef BOOL (^PMTextFieldOnEditing)(PMTextFieldDelegate *delegate, NSString *resultText, NSRange range, NSString *strNew);

@interface PMTextFieldDelegate : NSObject <UITextFieldDelegate>

+ (instancetype)delegateForTextField:(UITextField *)textField;

@property (nonatomic, weak) UITextField *textField;

- (void)onEditing:(PMTextFieldOnEditing)block;
- (void)onStartEditing:(PMTextFieldOnAction)block;
- (void)onEndEditing:(PMTextFieldOnAction)block;
- (void)onEnterButton:(PMTextFieldOnActionBOOL)block;
- (void)onClear:(PMTextFieldOnActionBOOL)block;

@end
