//
//  PMRoundedBlock.h
//  PxToday
//
//  Created by Yaroslav Babalich on 13.06.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "PMRoundedBlock.h"

@interface PMRoundedBlock ()
@property (nonatomic, assign) CGSize    oldFrameSize;

@end

@implementation PMRoundedBlock

#pragma mark -
#pragma mark Initial methods

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (!CGSizeEqualToSize(self.frame.size, self.oldFrameSize)) {
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath; // for speed up
        self.oldFrameSize = self.frame.size;
        [self setNeedsDisplay];
    }
}

#pragma mark -
#pragma mark Private methods

- (void)setup {
    //self.layer.borderWidth = 1.f;
    //self.layer.borderColor = LSRGBHex(0xfafafa).CGColor;
    self.layer.cornerRadius = 3.f;
    self.clipsToBounds = YES;
    [self updateShadow];
}

- (void)updateShadow {
    self.clipsToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowRadius = 3.0;
    self.layer.shadowOpacity = 0.2;
}

@end
