//
//  PMNavPanelButtonBack.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 1/26/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMNavPanelElement.h"

@interface PMNavPanelButtonBack : PMNavPanelElement

+ (instancetype)buttonWithTitle:(NSString *)title
                          onTap:(PMNavPanelElementOnTap)block;

@property (nonatomic, strong) NSString      *title;

@end
