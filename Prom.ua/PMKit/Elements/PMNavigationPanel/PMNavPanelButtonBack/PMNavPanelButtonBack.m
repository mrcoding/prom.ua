//
//  PMNavPanelButtonBack.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 1/26/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMNavPanelButtonBack.h"
#import "PMKit.h"

@interface PMNavPanelButtonBack ()
@property (weak, nonatomic) IBOutlet PMTouchesView      *containerView;
@property (weak, nonatomic) IBOutlet UIImageView        *imageView;
@property (weak, nonatomic) IBOutlet UILabel            *titleLabel;

@end

@implementation PMNavPanelButtonBack

#pragma mark -
#pragma mark Class Methods

+ (instancetype)buttonWithTitle:(NSString *)title onTap:(PMNavPanelElementOnTap)block {
    PMNavPanelButtonBack *element = [self element];
    element.titleLabel.text = title;
    [element onTap:block];
    return element;
}

- (void)awakeFromNib {
//    self.containerView.colorOnTouch = LSColorNavButtonBGHighlighted;
    [self.containerView onTap:^(id view, NSSet *touches) {
        if (self.onTapBlock) {
            self.onTapBlock(self);
        }
    }];
}

#pragma mark -
#pragma mark Accessors

- (NSString *)title {
    return self.titleLabel.text;
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}

@end
