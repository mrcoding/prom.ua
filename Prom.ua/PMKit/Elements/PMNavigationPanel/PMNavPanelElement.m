//
//  PMNavigationPanel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "PMNavPanelElement.h"
#import "PMKit.h"

@implementation PMNavPanelElement

#pragma mark -
#pragma mark Class Methods

+ (instancetype)element {
    PMNavPanelElement *element = [UINib loadClass:[self class]];
    element = element ? : [[self alloc] initWithRect:CGRectMake(0, 0, 50, 50)];
    element.enabledElement = YES;
    
    return element;
}

#pragma mark -
#pragma mark Public

- (void)onTap:(PMNavPanelElementOnTap)block {
    self.onTapBlock = block;
}


@end
