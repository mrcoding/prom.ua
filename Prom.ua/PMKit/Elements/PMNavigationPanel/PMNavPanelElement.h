//
//  PMNavigationPanel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PMNavPanelElementOnTap)(id view);

@interface PMNavPanelElement : UIView

+ (instancetype)element;

@property (nonatomic, copy) PMNavPanelElementOnTap          onTapBlock;
@property (nonatomic, assign) BOOL                          enabledElement;

- (void)onTap:(PMNavPanelElementOnTap)block;


@end
