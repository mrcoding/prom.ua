//
//  MJNavigationPanel.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSNavPanelElements.h"

@protocol PMNavigationPanelDelegate <NSObject>
@optional
- (void)navigationPanelBackBtnDidTaped;
- (void)navigationPanelLeftMenuBtnDidTaped;

@end

@interface PMNavigationPanel : UIView

@property (nonatomic, strong) IBOutlet  UIButton    *backBtn;
@property (nonatomic, strong) IBOutlet  UILabel     *titleLabel;
@property (nonatomic, strong) IBOutlet  UIView      *elementsContainerRight;
@property (nonatomic, strong) IBOutlet  UIView      *elementsContainerLeft;
@property (nonatomic, strong) IBOutlet  UIView      *mainContainerView;
@property (nonatomic, strong) IBOutlet  UIView      *secondContainerView;

@property (nonatomic, assign) id<PMNavigationPanelDelegate> delegate;
@property (nonatomic, strong) NSString              *title;

- (PMNavPanelButtonIcon *)addMenuButton; // button with menu icon and action - open left menu
- (PMNavPanelButtonBack *)addBackButton; // button with back icon and text, action - go back
- (PMNavPanelButtonIcon *)addSearchButtonWithCompletion:(void(^)()) completion; //button with back search icon
- (PMNavPanelButtonIcon *)addLogoButtonWithCompletion:(void(^)()) completion;

- (void)mainContainerVisible:(BOOL)visible animated:(BOOL)animated;
- (void)showSecondView:(UIView *)view; // main container view will be hidden

// all lsft and right elements
- (instancetype)removeElement:(PMNavPanelElement *)element; // searching in left and right elements
- (void)updateElementsAlign;

// right elements container
@property (nonatomic, readonly) NSArray             *elementsRight; // LSNavPanelElement

- (instancetype)addRightElement:(PMNavPanelElement *)element;
- (instancetype)addRightElements:(NSArray *)elements; // LSNavPanelElement
- (instancetype)removeAllRightElements;

// left elements container
@property (nonatomic, readonly) NSArray             *elementsLeft; // LSNavPanelElement

- (instancetype)addLeftElement:(PMNavPanelElement *)element;
- (instancetype)addLeftElements:(NSArray *)elements; // LSNavPanelElement
- (instancetype)removeAllLeftElements;


@end
