//
//  LSNavPanelButtonTextIconTop.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "PMNavPanelButtonTextIconTop.h"
#import "PMKit.h"

@interface PMNavPanelButtonTextIconTop ()
//@property (weak, nonatomic) IBOutlet LSTouchesView      *containerView;
@property (weak, nonatomic) IBOutlet UIImageView        *imageView;
//@property (weak, nonatomic) IBOutlet UILabel            *titleLabel;

@end

@implementation PMNavPanelButtonTextIconTop


@end
