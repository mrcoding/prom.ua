//
//  PMNavPanelButtonIcon.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 1/26/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMNavPanelElement.h"

@interface PMNavPanelButtonIcon : PMNavPanelElement

+ (instancetype)buttonWithIcon:(UIImage *)iconImage
                         onTap:(PMNavPanelElementOnTap)block;

@property (nonatomic, strong) UIImage       *iconImage;
@property (nonatomic, assign) CGFloat       iconSize; // size of icon in pixels

@end
