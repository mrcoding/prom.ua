//
//  PMNavPanelButtonIcon.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 1/26/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMNavPanelButtonIcon.h"
#import "PMKit.h"

@interface PMNavPanelButtonIcon ()
@property (weak, nonatomic) IBOutlet PMTouchesView      *containerView;
@property (weak, nonatomic) IBOutlet UIImageView        *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@end

@implementation PMNavPanelButtonIcon

#pragma mark -
#pragma mark Class Methods

+ (instancetype)buttonWithIcon:(UIImage *)iconImage
                         onTap:(PMNavPanelElementOnTap)block
{
    PMNavPanelButtonIcon *element = [self element];
    element.iconImage = iconImage;
    [element onTap:block];
    
    return element;
}

- (void)awakeFromNib {
//    self.containerView.colorOnTouch = LSColorNavButtonBGHighlighted;
    [self.containerView onTap:^(id view, NSSet *touches) {
        if (self.onTapBlock) {
            self.onTapBlock(self);
        }
    }];
}

#pragma mark -
#pragma mark Accessors

- (void)setIconImage:(UIImage *)iconImage {
    _iconImage = iconImage;
    self.imageView.image = iconImage;
}

- (void)setIconSize:(CGFloat)iconSize {
    self.heightConstraint.constant = floatInBounds(iconSize, 0, 100);
    [self setNeedsLayout];
}

@end
