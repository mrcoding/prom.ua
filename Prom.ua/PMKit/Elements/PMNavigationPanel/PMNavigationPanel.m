//
//  MJNavigationPanel.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//

#import "PMNavigationPanel.h"
#import "PMKit.h"
//#import "LSMainViewController.h"

@interface PMNavigationPanel()
@property (nonatomic, strong) NSMutableArray        *elementsRightMutable;
@property (nonatomic, strong) NSMutableArray        *elementsLeftMutable;

@end

@implementation PMNavigationPanel

@dynamic title;
@dynamic elementsRight;
@dynamic elementsLeft;

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = LSColorNormal;
    }
    return self;
}

#pragma mark -
#pragma mark Accessors

- (void)setBackBtn:(UIButton *)backBtn {
    _backBtn = backBtn;
    [self addTargetForBackBtn];
}

- (NSString *)title {
    return self.titleLabel.text;
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}

- (NSMutableArray *)elementsRightMutable {
    if (!_elementsRightMutable) {
        _elementsRightMutable = [NSMutableArray array];
    }
    
    return _elementsRightMutable;
}

- (NSArray *)elementsRight {
    return [self.elementsRightMutable copy];
}

- (NSMutableArray *)elementsLeftMutable {
    if (!_elementsLeftMutable) {
        _elementsLeftMutable = [NSMutableArray array];
    }
    
    return _elementsLeftMutable;
}

- (NSArray *)elementsLeft {
    return [self.elementsLeftMutable copy];
}

- (void)setMainContainerView:(UIView *)mainContainerView {
    _mainContainerView = mainContainerView;
    mainContainerView.hidden = NO; // for hiding will be used alpha
    [self mainContainerVisible:YES animated:NO];
}

- (void)setSecondContainerView:(UIView *)secondContainerView {
    _secondContainerView = secondContainerView;
    secondContainerView.hidden = NO; // for hiding will be used alpha
    [self mainContainerVisible:YES animated:NO];
}

#pragma mark -
#pragma mark Public methods

- (void)mainContainerVisible:(BOOL)visible animated:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.1 : 0.0 animations:^{
        self.mainContainerView.alpha = visible ? 1.0 : 0.0;
        self.secondContainerView.alpha = visible ? 0.0 : 1.0;
    } completion:^(BOOL finished) {
        if (visible && finished) {
            [self.secondContainerView removeAllSubviews];
        }
    }];
}

- (void)showSecondView:(UIView *)view {
    if (!view) {
        [self mainContainerVisible:YES animated:YES];
        return;
    }
    
    [self.secondContainerView removeAllSubviews];
    [self.secondContainerView addSubview:view];
    [view alignExpandToSuperview];
    [self mainContainerVisible:NO animated:YES];
}

// standard elements

- (PMNavPanelButtonIcon *)addMenuButton {
    PMNavPanelButtonIcon *button = [PMNavPanelButtonIcon buttonWithIcon:[UIImage imageNamed:@"ic_menu"]
                                                                  onTap:^(id view)
                                    {
//                                        [[LSNavigation navigation].mainViewController showLeftMenu];
                                    }];
    button.iconSize = 23.0;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:button
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                     toItem:nil
                                                                  attribute:0
                                                                 multiplier:1.0
                                                                   constant:60];
    [button addConstraint:constraint];
    [self addLeftElement:button];
    
    return button;
}

- (PMNavPanelButtonBack *)addBackButton {
    NSString *title = NSLocalizedString(@"navigation.goBack", nil);
    PMNavPanelButtonBack *button = [PMNavPanelButtonBack buttonWithTitle:title
                                                                   onTap:^(id view) {
                                                                       [[MGBaseNavigation navigation] goBack];
                                                                   }];
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:button
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                      toItem:nil
                                                                   attribute:0
                                                                  multiplier:1.0
                                                                    constant:60];
    [button addConstraint:constraint];
    [self addLeftElement:button];
    
    return button;
}

- (PMNavPanelButtonIcon *)addSearchButtonWithCompletion:(void(^)()) completion{
    PMNavPanelButtonIcon *button = [PMNavPanelButtonIcon buttonWithIcon:[UIImage imageNamed:@"ic_search"]
                                                                  onTap:^(id view)
                                    {
                                        if (completion) {
                                            completion();
                                        }
                                    }];
    [self addRightElement:button];
    
    return button;
}

- (PMNavPanelButtonIcon *)addLogoButtonWithCompletion:(void(^)()) completion{
    PMNavPanelButtonIcon *button = [PMNavPanelButtonIcon buttonWithIcon:[UIImage imageNamed:@"ic_prom_logo"]
                                                                  onTap:^(id view)
                                    {
                                        if (completion) {
                                            completion();
                                        }
                                    }];
    button.iconSize = 70.f;
    [self addLeftElement:button];
    
    return button;
}

// right elements methods

- (instancetype)addRightElement:(PMNavPanelElement *)element {
    return [self addRightElements: element ? @[element] : nil];
}

- (instancetype)addRightElements:(NSArray *)elements {
    if (elements.count == 0) {
        return self;
    }
    
    [NSThread performSynchonouslyOnMainThread:^{
        [self.elementsRightMutable addObjectsFromArray:elements];
        [self updateElementsAlign];
    }];
    
    return self;
}

- (instancetype)removeAllRightElements {
    [NSThread performSynchonouslyOnMainThread:^{
        self.elementsRightMutable = nil;
        [self updateElementsAlign];
    }];
    
    return self;
}

// left elements methods

- (instancetype)addLeftElement:(PMNavPanelElement *)element {
    return [self addLeftElements: element ? @[element] : nil];
}

- (instancetype)addLeftElements:(NSArray *)elements {
    if (elements.count == 0) {
        return self;
    }
    
    [NSThread performSynchonouslyOnMainThread:^{
        [self.elementsLeftMutable addObjectsFromArray:elements];
        [self updateElementsAlign];
    }];
    
    return self;
}

- (instancetype)removeAllLeftElements {
    [NSThread performSynchonouslyOnMainThread:^{
        self.elementsLeftMutable = nil;
        [self updateElementsAlign];
    }];
    
    return self;
}

// all elements

- (instancetype)removeElement:(PMNavPanelElement *)element {
    if (!element) {
        return self;
    }
    
    [NSThread performSynchonouslyOnMainThread:^{
        [self.elementsRightMutable removeObject:element];
        [self.elementsLeftMutable removeObject:element];
        [self updateElementsAlign];
    }];
    
    return self;
}

// align methods

- (void)updateElementsAlign {
    [NSThread performSynchonouslyOnMainThread:^{
        NSArray *elementsRight = [self.elementsRight filteredArrayWithBlock:^BOOL(PMNavPanelElement *view) {
            return !view.hidden && view.alpha != 0;
        }];
        NSArray *elementsLeft = [self.elementsLeft filteredArrayWithBlock:^BOOL(PMNavPanelElement *view) {
            return !view.hidden && view.alpha != 0;
        }];
        
        [self alignElements:elementsRight toView:self.elementsContainerRight];
        [self alignElements:elementsLeft toView:self.elementsContainerLeft];
    }];
}

- (void)alignElements:(NSArray *)elements toView:(UIView *)containerView {
    if (!containerView) {
        return;
    }
    
    [containerView removeAllSubviews];
    
    UIView *leftView = nil;
    for (UIView *view in elements) {
        [containerView addSubview:view];
        [view alignTopToView:containerView toTop:YES padding:0];
        [view alignBottomToView:containerView toBottom:YES padding:0];
        [view alignLeftToView:leftView ? : containerView toLeft:!leftView padding:leftView ? 0 : 0];
        leftView = view;
    }
    [leftView alignRightToView:containerView toRight:YES padding:0];
}

#pragma mark -
#pragma mark Private methods

- (void)addTargetForBackBtn {
    if (!self.backBtn) return;
    [self.backBtn addTarget:self action:@selector(backBtnTouchUpInsideEvent:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -
#pragma mark Events

- (void)backBtnTouchUpInsideEvent:(UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(navigationPanelBackBtnDidTaped)]) {
        [self.delegate navigationPanelBackBtnDidTaped];
    }
}

- (void)leftBtnTouchUpInsideEvent:(UIButton *)btn {
    if ([self.delegate respondsToSelector:@selector(navigationPanelLeftMenuBtnDidTaped)]) {
        [self.delegate navigationPanelLeftMenuBtnDidTaped];
    }
}

@end
