//
//  PMNavigationPanel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//


#import "PMNavPanelElement.h"

@interface PMNavPanelButtonTextIcon : PMNavPanelElement

+ (instancetype)buttonWithTitle:(NSString *)title
                           icon:(UIImage *)iconImage
                          onTap:(PMNavPanelElementOnTap)block;

@property (nonatomic, strong) NSString          *title;
@property (nonatomic, strong) UIImage           *iconImage;

@property (weak, nonatomic) IBOutlet UILabel    *titleLabel;

//- (void)onTap();

@end
