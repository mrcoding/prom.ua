//
//  PMNavigationPanel.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 08.10.15.
//  Copyright © 2015 PxToday. All rights reserved.
//
#import "PMNavPanelButtonTextIcon.h"
#import "PMKit.h"

@interface PMNavPanelButtonTextIcon ()
//@property (weak, nonatomic) IBOutlet LSTouchesView      *containerView;
@property (weak, nonatomic) IBOutlet UIImageView        *imageView;


@end

@implementation PMNavPanelButtonTextIcon

#pragma mark -
#pragma mark Class Methods

+ (instancetype)buttonWithTitle:(NSString *)title icon:(UIImage *)iconImage onTap:(PMNavPanelElementOnTap)block {
    PMNavPanelButtonTextIcon *element = [self element];
    element.title = title;
    element.iconImage = iconImage ? : [UIImage imageNamed:@"ic_button_ok_white"];
    element.titleLabel.textColor = [UIColor whiteColor];
    [element onTap:block];
    
    return element;
}

- (void)awakeFromNib {
//    self.containerView.colorOnTouch = LSColorNavButtonBGHighlighted;
//    [self.containerView onTap:^(id view, NSSet *touches) {
//        if (self.onTapBlock && self.enabledElement) {
//            self.onTapBlock(self);
//        }
//    }];
}

#pragma mark -
#pragma mark Accessors

- (void)setEnabledElement:(BOOL)enabledElement {
    [super setEnabledElement:enabledElement];
//    [NSThread performSynchonouslyOnMainThread:^{
        if (enabledElement) {
            self.imageView.alpha = 1;
            self.titleLabel.alpha = 1;
        } else {
            CGFloat alpha = 0.5;
            self.imageView.alpha = alpha;
            self.titleLabel.alpha = alpha;
        }
//    }];
}

- (NSString *)title {
    return self.titleLabel.text;
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}

- (void)setIconImage:(UIImage *)iconImage {
    _iconImage = iconImage;
    self.imageView.image = iconImage;
}

@end
