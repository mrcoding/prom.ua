//
//  PMNavPanelSearchView.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 2/10/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PMNavPanelSearchViewTapBlock)(id view);

@interface PMNavPanelSearchView : UIView

+ (instancetype)view;

@property (nonatomic, strong) NSString      *text;
@property (nonatomic, strong) NSString      *placeholderText;

- (void)setFocused;
- (void)closeView;
- (void)onClose:(PMNavPanelSearchViewTapBlock)block;
- (void)onChanged:(PMNavPanelSearchViewTapBlock)block;

@end
