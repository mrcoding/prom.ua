//
//  PMNavPanelSearchView.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 2/10/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "PMNavPanelSearchView.h"
#import "PMKit.h"

@interface PMNavPanelSearchView ()
@property (weak, nonatomic) IBOutlet PMTouchesView      *learchButton;
@property (weak, nonatomic) IBOutlet PMTouchesView      *closeButton;
@property (weak, nonatomic) IBOutlet PMTextField        *textField;

@property (nonatomic, copy) PMNavPanelSearchViewTapBlock   onCloseBlock;
@property (nonatomic, copy) PMNavPanelSearchViewTapBlock   onChangedBlock;

@end

@implementation PMNavPanelSearchView

@synthesize text = _text;

#pragma mark -
#pragma mark Class Methods

+ (instancetype)view {
    return [UINib loadClass:[self class]];
}

#pragma mark -
#pragma mark Life Cycle

- (void)awakeFromNib {
    selfWeakCreate;
    PMTextField *textfield = self.textField;
    textfield.text = nil;
    
    PMTextFieldDelegate *delegate = textfield.delegateObject;
    [delegate onEditing:^BOOL(PMTextFieldDelegate *delegate, NSString *resultText, NSRange range, NSString *strNew) {
        _text = resultText;
        if (selfWeak.onChangedBlock) {
            selfWeak.onChangedBlock(selfWeak);
        }
        return YES;
    }];
    [delegate onEnterButton:^BOOL(PMTextFieldDelegate *delegate) {
        return YES;
    }];
    [delegate onClear:^BOOL(PMTextFieldDelegate *delegate) {
        return YES;
    }];
    
    [self.closeButton onTap:^(id view, NSSet *touches) {
        [selfWeak closeView];
    }];
}

#pragma mark -
#pragma mark Accessors

- (void)setText:(NSString *)text {
    _text = text;
    [NSThread performSynchonouslyOnMainThread:^{
        self.textField.text = text;
    }];
}

- (void)setPlaceholderText:(NSString *)placeholderText {
    [NSThread performSynchonouslyOnMainThread:^{
        self.textField.placeholder = placeholderText;
    }];
}

- (NSString *)placeholderText {
    return self.textField.placeholder;
}

#pragma mark -
#pragma mark Public

- (void)setFocused {
    [self.textField becomeFirstResponder];
}

- (void)closeView {
    if (self.onCloseBlock) {
        self.onCloseBlock(self);
    }
}

- (void)onClose:(PMNavPanelSearchViewTapBlock)block {
    self.onCloseBlock = block;
}

- (void)onChanged:(PMNavPanelSearchViewTapBlock)block {
    self.onChangedBlock = block;
}

@end
