//
//  MGImageView.h
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/24/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MGImageModel;

@interface MGImageView : UIView

@property (nonatomic, strong) UIImage               *backgroundImage;
@property (nonatomic, strong) NSURL                 *imageURL;
@property (nonatomic, strong) MGImageModel          *imageModel;

@end
