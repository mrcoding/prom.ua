//
//  MGImageView.m
//  MyJeweler
//
//  Created by Yaroslav Babalich on 5/24/16.
//  Copyright © 2016 PxToday. All rights reserved.
//

#import "MGImageView.h"
#import "PMKit.h"

@interface MGImageView() <MGImageModelProtocol>

@property (nonatomic, assign) CGSize oldFrameSize;

@end

@implementation MGImageView

#pragma mark -
#pragma mark Initialize and dealloc methods

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (!CGSizeEqualToSize(self.frame.size, self.oldFrameSize)) {
        self.oldFrameSize = self.frame.size;
        [self setNeedsDisplay];
    }
}

- (void)dealloc {
    self.imageModel = nil; // remove observer
}

#pragma mark -
#pragma mark Accessors
- (void)setBackgroundImage:(UIImage *)backgroundImage {
    if (_backgroundImage != backgroundImage) {
        _backgroundImage = backgroundImage;
        [self setNeedsDisplay];
    }
}

- (void)setImageURL:(NSURL *)imageURL {
    _imageURL = imageURL;
    [self.imageModel cancel];
    self.imageModel = nil;
    if (imageURL) {
        self.imageModel = [MGImageModel imageWithURL:imageURL];
    }
}

- (void)setImageModel:(MGImageModel *)imageModel {
    self.backgroundImage = nil;
    if (imageModel != _imageModel) {
        [_imageModel cancel];
        [_imageModel removeObserver:self];
        _imageModel = imageModel;
        [_imageModel addObserver:self];
        
        //self.loadingActive = NO;
        
        if (_imageModel.image) {
            [self imageModelDidLoad:_imageModel];
        } else if (_imageModel) {
            //self.loadingActive = YES;
            [_imageModel load];
        }
    }
}


- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    
    CGContextSaveGState(context);
    
    UIImage *image = (self.backgroundImage) ? : [UIImage imageNamed:@"ic_ware"]; ;
     if (image) {
        [image drawInRect:rect];
    }
    
    CGContextRestoreGState(context);
}

#pragma mark -
#pragma mark Private methods
- (void)setup{
    self.backgroundImage = [UIImage imageNamed:@"ic_ware"];
    self.backgroundColor = [UIColor clearColor];
}

#pragma mark -
#pragma mark Private <MGImageModelProtocol>

- (void)imageModelDidLoad:(MGImageModel *)imageModel {
    if (self.imageModel == imageModel) {
        UIImage *image = self.imageModel.image;
        self.backgroundImage = image;
    }
}

- (void)imageModelDidCancelLoading:(MGImageModel *)imageModel {
    if (self.imageModel == imageModel) {
        self.backgroundImage = nil;
    }
}

@end
