//
//  PMTouchesView.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 9/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PMTouchesViewBlock)(id view, NSSet /* UITouch */ *touches , UITouchPhase type);
typedef void (^PMTouchesViewTapBlock)(id view, NSSet /* UITouch */ *touches);

@interface PMTouchesView : UIView

/**
 * Catch tapes and moves on current view and stop notification propagation to parent views. 
 * Default - NO.
 */
@property (nonatomic, assign) BOOL                  notSendToParentViews;
@property (nonatomic, strong) UIColor               *colorOnTouch;

@property (nonatomic, assign) UIResponder           *delegateForTouches;


- (void)onTouches:(PMTouchesViewBlock)block;
- (void)onTap:(PMTouchesViewTapBlock)block;
//- (void)onLongTap:(LSTouchesViewTapBlock)block;

@end
