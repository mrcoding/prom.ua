//
//  PMTouchesView.h
//  Prom.ua
//
//  Created by Yaroslav Babalich on 9/8/15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#import "PMTouchesView.h"
#import "PMKit.h"

static NSTimeInterval const kLSTouchesViewLongTapTime = 1.0;

@interface PMTouchesView ()
@property (nonatomic, strong) PMTouchesViewBlock        onTouchesBlock;
@property (nonatomic, strong) PMTouchesViewTapBlock     onTapBlock;
@property (nonatomic, strong) PMTouchesViewTapBlock     onLongTapBlock;
@property (nonatomic, strong) UIColor                   *colorNormal;
@property (nonatomic, assign) CGPoint                   startTapPoint;
@property (nonatomic, assign) NSTimeInterval            startTapTimestamp;

@end

@implementation PMTouchesView

#pragma mark -
#pragma mark Public

- (void)onTouches:(PMTouchesViewBlock)block {
    self.onTouchesBlock = block;
}

- (void)onTap:(PMTouchesViewTapBlock)block {
    self.onTapBlock = block;
}

- (void)onLongTap:(PMTouchesViewTapBlock)block {
    self.onLongTapBlock = block;
}

#pragma mark -
#pragma mark Accessors 

- (void)setColorOnTouch:(UIColor *)colorOnTouch {
    self.colorNormal = self.backgroundColor ? : [UIColor clearColor];
    _colorOnTouch = colorOnTouch;
}

#pragma mark -
#pragma mark Private Actions

- (void)notifyWithTouches:(NSSet *)touches type:(UITouchPhase)type {
    if (self.colorOnTouch) {
        self.backgroundColor = self.colorOnTouch;
    }
    
    if (type == UITouchPhaseBegan) {
        UITouch *touch = touches.allObjects.firstObject;
        self.startTapPoint = [touch locationInView:self];
        self.startTapTimestamp = touch.timestamp;
    }
    
    if (type == UITouchPhaseEnded || type == UITouchPhaseCancelled) {
        if (self.colorOnTouch) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.backgroundColor = self.colorOnTouch;
                [UIView animateWithDuration:0.2 delay:0.0 options:0
                                 animations:^{
                                     self.backgroundColor = self.colorNormal;
                                 } completion:nil];
            });
        }
    }
    
    if (self.onTouchesBlock) {
        self.onTouchesBlock(self, touches, type);
    }
    
    if (type == UITouchPhaseEnded) {
        CGPoint point2 = self.startTapPoint;
        NSTimeInterval startTimestamp = self.startTapTimestamp;
        
        if (self.onTapBlock) {
            for (UITouch *touch in touches) {
                CGPoint point = [touch locationInView:self];
                BOOL longTap = touch.timestamp - startTimestamp >= kLSTouchesViewLongTapTime;
                
                if (self.onLongTapBlock && longTap) {
                    self.onLongTapBlock(self, touches);
                } else {
                    if (distanceBetweenPoints(point, point2) < 10) {
                        self.onTapBlock(self, touches);
                        break;
                    }
                }
            }
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.notSendToParentViews) {
        [super touchesBegan:touches withEvent:event];
    }
    [self notifyWithTouches:touches type:UITouchPhaseBegan];
    [self.delegateForTouches touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.notSendToParentViews) {
        [super touchesCancelled:touches withEvent:event];
    }
    [self notifyWithTouches:touches type:UITouchPhaseCancelled];
    [self.delegateForTouches touchesCancelled:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.notSendToParentViews) {
        [super touchesMoved:touches withEvent:event];
    }
    [self notifyWithTouches:touches type:UITouchPhaseMoved];
    [self.delegateForTouches touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.notSendToParentViews) {
        [super touchesEnded:touches withEvent:event];
    }
    [self notifyWithTouches:touches type:UITouchPhaseEnded];
    [self.delegateForTouches touchesEnded:touches withEvent:event];
}

@end
