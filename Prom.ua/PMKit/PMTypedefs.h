//
//  PMKit.h
//  Proma.ua
//
//  Created by Yaroslav Babalich on 27.05.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//


#ifndef PMTypedefs_h
#define PMTypedefs_h

#define PMSiteURL @"https://my.prom.ua/"

#define LSColorNormal LSRGBHex(0x00B9EA);

#endif /* PMTypedefs_h */


typedef NS_ENUM(NSInteger, PMOrderState){
    kPMOrderStateUnknown = 0,
    kPMOrderStateNew,
    kPMOrderStateAccepted,
    kPMOrderStateDeclined,
    kPMOrderStateDraft,
    kPMOrderStateClosed,
};

