//
//  PMKit.h
//  Proma.ua
//
//  Created by Yaroslav Babalich on 27.05.15.
//  Copyright (c) 2015 PxToday. All rights reserved.
//

#ifndef Calendar_Defines_h
#define Calendar_Defines_h

#define LWCoreDataStackName @"LearnWords";

#define MGLog();
#define MGLogFrame(view) NSLog(@"Class -> [%@] frame = [%@]" , NSStringFromClass(view.class), NSStringFromCGRect(view.frame));
#define MGLogBounds(view) NSLog(@"Class -> [%@] bounds = [%@]" , NSStringFromClass(view.class), NSStringFromCGRect(view.bounds));
#define MGLogPoint(point) NSLog(@"point = [%@]", NSStringFromCGPoint(point));
#define MGLogViewCenter(view) NSLog(@"Class -> [%@] center = [%@]" , NSStringFromClass(view.class), NSStringFromCGPoint(view.center));
#define MGLogInteger(string, integer) NSLog(@"String -> [%@] integer = [%li]" , string, (long)integer);
#define MGLogArray(string, array) NSLog(@"String -> [%@] array = [%@]" , string, array.description);

#endif
